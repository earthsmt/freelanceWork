<?php
set_time_limit(0);
error_reporting(E_ERROR | E_PARSE);
function scrapSite($url)
{
	if(is_string($url))
	{
		$ch = curl_init($url);

		$userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0';
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept-Language: en-US;en;q=0.5"));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_ENCODING,  '');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_ENCODING, '');
		//curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		curl_setopt( $ch, CURLOPT_USERAGENT, $userAgent );
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
	return false;
}

function scrapContentFromHtml($query, $html, $partHtml)
{
	if(!empty($html))
	{
		$dom = new DOMDocument();
		libxml_use_internal_errors(true);
		$dom->loadHTML($html);
		$xpath = new DOMXpath($dom);
		if(!$partHtml)
		{
			$row = $xpath->query($query);
			foreach($row as $value)
			{
				$values[] = trim($value->textContent);
			}
		}
		else
		{
			$childNode = $xpath->evaluate($query);
			foreach ($childNode as $child) 
			{ 
				$innerHTML = $child->ownerDocument->saveHtml($child);
				if(($partHtml != "allHtml") && strpos($innerHTML, $partHtml) !== false)
				{
					$values = $innerHTML;
					return $values;
				}
				elseif($partHtml == "allHtml")
				{
					$values[] = $innerHTML;
				}
			}
		}
		return !isset($values) ? false : $values;
	}
	return false;
}

function setUpCsvFile($name, $fieldHeaders = array())
{
	$fileName = $name.".csv";
	if(!file_exists($fileName))
	{
		$fileHolder = fopen($fileName,"w");
		fputcsv($fileHolder, $fieldHeaders);
	}
	else
	{
		$fileHolder = fopen($fileName,"a+");
	}
	return $fileHolder;
}

function readCsvFile($name){
	$dateBound = (time() - (60*60*24));
	$fileName = $name.".csv";
	if(file_exists($fileName))
	{
		$file_handle = fopen($fileName, 'r');
		$counter = 0;
		while (!feof($file_handle) ) {
			$line = fgetcsv($file_handle, 1024);
			if(is_array($line)  && $counter > 0)
			{
				if(strtotime($line[0]) < $dateBound)
				{
					continue;
				}
				$implodeData = $line;
				unset($implodeData[0]);
				$keys[] = sha1(implode(",", $implodeData));
			}
			$line_of_text[] = $line;
			$counter++;
		}
		fclose($file_handle);
		return array($line_of_text, $keys);
	}
	return false;
}

function main()
{
	$fileHolder = setUpCsvFile('scraperLadbrokesResults', array('Date Scraped', 'Event Time', 'Race Info Name',
		'Race Info Number', 'Race Number', 'Racer Name', 'Odd 1', 'Odd 2'));
	$raceKeys = readCsvFile('scraperLadbrokesResults')[1];
		
	$response = scrapSite('https://www.ladbrokes.com.au/racing/horses/');
	$horseRacingCollection = scrapContentFromHtml('//li[contains(@class, "has-children")]', $response, true);
	if(is_array($horseRacingCollection))
	{
		array_push($horseRacingCollection, array_shift($horseRacingCollection));
		foreach($horseRacingCollection as $horseRacing)
		{
			if(strpos($horseRacing, '<span>Australia')!==false)
			{
				$horseRaceCollectionToObserve = $horseRacing;
				break;		
			}		
		}
	}
	if(isset($horseRaceCollectionToObserve))
	{
		$racesToObserve = scrapContentFromHtml('//a[span[contains(text(), "Race")]]/@href', $horseRaceCollectionToObserve, false);
		foreach($racesToObserve as &$raceToObserve)
		{
			$raceToObserve = 'https://www.ladbrokes.com.au'.$raceToObserve;
			//$raceToObserve = 'https://www.ladbrokes.com.au/racing/horses/gulfstream-park/48808655-race-12/';
			$response = scrapSite($raceToObserve);

			$titleInfo = scrapContentFromHtml('//title', $response, false);
			$titleInfo = substr($titleInfo[0], strpos($titleInfo[0], '-')+2);
			$titleInfo = substr($titleInfo, 0, strpos($titleInfo, '-'));
			$raceInfoName = trim(substr($titleInfo, 0, strpos($titleInfo, 'Race')));
			$raceInfoNumber = strtoupper(trim(substr($titleInfo, strpos($titleInfo, 'Race'))));
			$time = scrapContentFromHtml('//span[@data-localtime]/@data-localtime', $response, false);
			$time = isset($time[0]) && is_numeric($time[0]) ? date('H:i', $time[0]) : '-';
		
			$raceDetails = scrapContentFromHtml('//td[contains(@class, "entrant data") and .//div[@class]]', $response, true);
			$winOdds = scrapContentFromHtml('//td[contains(@class, "win odds") and .//div]', $response, true);
			$flucOdds = scrapContentFromHtml('//td[contains(@class, "flucs subcontent subcontent-default subcontent-results subcontent-fullresults subcontent-winplace") and .//span]', $response, false);
			$raceData = array();
			$counter = 0;
		
			if(is_array($winOdds))
			{
				foreach($winOdds as $key => $winOdd)
				{
					$winOddsData = scrapContentFromHtml('//span[@data-last_update]', $winOdd, false);
					$racer = scrapContentFromHtml('//span[@class="competitor-name"]', $raceDetails[$key], false);
					$raceNumber = scrapContentFromHtml('//span[@class="saddle-number"]', $raceDetails[$key], false);
					
					if(!is_numeric($winOddsData[0]))
					{
						$raceData[$key]['odd'] = '-';
						$raceData[$key]['odd_2'] = '-';
						$raceData[$key]['win']  = $winOddsData[0];
					}
					elseif(isset($flucOdds[$counter]))
					{
						$flucOddData = explode(',', $flucOdds[$counter]);
						$raceData[$key]['odd'] = isset($flucOddData[0]) && is_numeric($flucOddData[0]) ? trim($flucOddData[0]) : 0;
						$raceData[$key]['odd_2'] = isset($flucOddData[1]) && is_numeric($flucOddData[1]) ? trim($flucOddData[1]) : 0;
						$raceData[$key]['win']  = $winOddsData[0];

						if(!empty($raceData[$key]['odd_2']) && !empty($raceData[$key]['odd']) && (($raceData[$key]['odd_2'] <= 0.8*$raceData[$key]['odd'])))
						{
							$currentTimestamp = date('Y-m-d H:i:s', time());
							//print('"'.$time.'" "'.$raceInfoName.'" "'.$raceInfoNumber.'" "'.$raceNumber[0].'" "'.$racer[0].'" "$'.$raceData[$key]['odd'].'" "'.$raceData[$key]['odd_2'].'"'.PHP_EOL);
							
							$element = $time.','.$raceInfoName.','.$raceInfoNumber.','.$raceNumber[0].','.$racer[0].',$'.$raceData[$key]['odd'].','.$raceData[$key]['odd_2'];
							if(!in_array(sha1($element), $raceKeys))
							{
								$csvDetails = array($currentTimestamp, $time, $raceInfoName, $raceInfoNumber, $raceNumber[0], $racer[0], '$'.$raceData[$key]['odd'], $raceData[$key]['odd_2']);
								fputcsv($fileHolder, $csvDetails);
							}
							/*
							else
							{
								var_dump($element);
								die();
							}
							*/
						}
					}
					$counter++;
				}	
			}
		}
		fclose($fileHolder);
		return;
	}
}

?>