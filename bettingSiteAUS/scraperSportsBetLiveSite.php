<html>
<meta http-equiv="refresh" content="20">
<head>
	<title> SportsBet Scores Website </title>
</head>
<body>
<p> Last Update at: <?php print_r(date('d-m-Y H:i:s', time()));?> </p>
<?php
$file = 'scraperSportsBetResults';
$results = readCsvFileForTableOutput($file);
if(!empty($results) && is_array($results))
{
	print_r(buildHtmlTable($results));
}
function readCsvFileForTableOutput($name){
	$dateBound = (time() - (60*60*48));
	$line_of_text = array();
	$fileName = $name.".csv";
	if(file_exists($fileName))
	{
		$file_handle = fopen($fileName, 'r');
		while (!feof($file_handle) ) {
			$line = fgetcsv($file_handle, 1024);
			if(count($line_of_text) >=1)
			{
				if(strtotime($line[0]) < $dateBound)
				{
					continue;
				}
			}
			$line_of_text[] = $line;
		}
		fclose($file_handle);
		$headers = array($line_of_text[0]);
		unset($line_of_text[0]);
		return array_merge($headers, array_reverse($line_of_text));
	}
	return false;
}

function buildHtmlTable($array){
    $html = '<table border="1" centre="1">';
    $html .= '<tr>';
    foreach($array[0] as $key=>$value){
            $html .= '<th>' . htmlspecialchars($value) . '</th>';
        }
    $html .= '</tr>';
    foreach( $array as $key=>$value){
		if(is_array($value) && $key > 0)
		{
			$html .= '<tr>';
			foreach($value as $key2=>$value2){
				$html .= '<td>' . htmlspecialchars($value2) . '</td>';
			}
			$html .= '</tr>';
		}
    }
    $html .= '</table>';
    return $html;
}
?>
</body>
</html>