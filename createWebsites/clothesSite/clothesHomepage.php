<?php
     session_start();
?>

<html>
<head>
	<title> Shopping Site </title>
</head>
<body>
<p id='start' align='center'> Welcome to the Shopping Site </p>
<hr />
<p align="center"><button type="button" onclick="addForm()" style="width:400px;height:120px;">Place Order</button></p><p>
<br /> 
<p align="center"><button type="button" onclick="searchForm()" style="width:400px;height:120px;">Viewing Orders</button></p>
<hr/>
 
<div id="showForm" style="display: none;">
<p id='header'>New Shopping Order:</p>
<form action="submit.php" method="post" id="formId" target="_self"> 
	<select name="title" name="title" id="title">
    <option value="Mr">Mr</option>
    <option value="Mrs">Mrs</option>
  </select><br /><br />
	First Name:<br /><input type="text" id="first_name" name="first_name" placeholder='Please enter first name'/><br/><br/>
	Surname:<br /><input type="text" id="last_name" name="last_name" placeholder='Please enter surname'"/><br/><br/>
	Age:<br /><input id="number" id="age" name="age" type="number"><br /><br />
	Ordering:<br />
    <select onchange="changeForm()" name="ordering" id="ordering" name="ordering">
    <option value="Shirt">Shirt</option>
    <option value="Cap">Cap</option>
	<option value="Trousers">Trousers</option>
     </select><br /><br />
	Size:<select onchange="changeForm()" name="size" id="size" name="size">
    <option value="S">S</option>
	<option value="L">L</option>
    </select><br /><br />
	Price:<br /><input type="text" id="price" name="price" placeholder='' readonly="readonly" value=''/><br/><br/>
	<div style="position:relative; left:200px;bottom:305px">Address: <br /><TEXTAREA NAME="address" id="address" ROWS=10 COLS=30></TEXTAREA></div>
	<input type="Submit" name="Submit" style="position:relative; bottom:205px" value='Submit'/>
</form>
<hr style="position:relative; bottom:205px"/>
</div>
<div id='previous'>
<p align = 'center' >
<?php
	if(isset($_SESSION['recent_member']))
	{
		echo $_SESSION['recent_member'];		
	}
	
	if(isset($_SESSION["query_results"]))
	{
		echo $_SESSION["query_results"];
	}
	
	if(isset($_SESSION["modify_results"]))
	{
		echo $_SESSION["modify_results"];
	}
?>
</p>
</div>
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script>
function changeForm(id=false)
{
	if(id)
	{
		var a = "modifyOption_".concat(id);
		var a = a.concat("_size");
		var b = "modifyOption_".concat(id);
		var b = b.concat("_ordering");
		var ddl = document.getElementById(a);
		var size = ddl.options[ddl.selectedIndex].value;
		var ddl = document.getElementById(b);
		var ordering = ddl.options[ddl.selectedIndex].value;
	}
	else
	{
		var ddl = document.getElementById("size");
		var size = ddl.options[ddl.selectedIndex].value;
		
		var ddl = document.getElementById("ordering");
		var ordering = ddl.options[ddl.selectedIndex].value;
	}

	var price = document.getElementById("price");
	if(ordering=='Shirt')
	{
		if(size=='S')
		{
			cost = '£13.50';
			document.getElementById("price").value='£13.50';
		}
		if(size=='L')
		{
			cost = '£17.50';
			document.getElementById("price").value='£17.50';
		}
	}
	else if(ordering=='Cap')
	{
		if(size=='S')
		{
			cost = '£11.50';
			document.getElementById("price").value='£11.50';
		}
		if(size=='L')
		{
			cost = '£12.50';
			document.getElementById("price").value='£12.50';
		}
	}
	else if(ordering=='Trousers')
	{
		if(size=='S')
		{
			cost = '£34.50';
			document.getElementById("price").value='£34.50';
		}
		if(size=='L')
		{
			cost = '£37.50';
			document.getElementById("price").value='£37.50';
		}
	}
	return cost;
}

function addForm() {
	document.getElementById("showForm").style.display = "block";
	document.getElementById("previous").style.position = "relative";
	document.getElementById("previous").style.bottom = "205px";
    document.getElementById("header").innerHTML = "NEW ORDER:";
	document.getElementById("formId").action = "newOrder.php";
}
function searchForm(){
	document.getElementById("showForm").style.display = "block";
	document.getElementById("previous").style.position = "relative";
	document.getElementById("previous").style.bottom = "205px";
	document.getElementById("first_name").setAttribute("onclick", "showHint(this)");
	document.getElementById("last_name").setAttribute("onclick", "showHint(this)");
	document.getElementById("address").setAttribute("onclick", "showHint(this)");
	document.getElementById("header").innerHTML = "Searching ORDERS:";
	document.getElementById("formId").action = "viewingOrders.php";
}

function detectChange(row){
	var elementId = 'deleteOption_'.concat(row);
	var x = document.getElementById(elementId);
	x.setAttribute("value", "0");
	x.setAttribute("type", "hidden");
	var cost = changeForm(row);
	var elementId = 'modifyOption_'.concat(row);
	elementId = elementId.concat('_price');
	var x = document.getElementById(elementId);
	x.setAttribute("value", cost);
}

 function showHint(obj) {
	 
	if(obj.value!="")
	{
		var values = { };
		values[obj.id] = obj.value;
		values['ajaxMode'] = true;
		$.ajax({
			   url: "/echo/html/",
               type: "POST",
			   cache:'false',
               url: "viewingOrders.php",
               data: values,
			   async: false,
			   dataType: "html",
               success: function(data) {
				   var suggestion = $(data).html();
				   if(suggestion != null)
				   {
					   obj.value = suggestion;
				   }
               }
           });
	}
 }

</script>

</body>


