<?php 
session_start();

$servername = "localhost";
$username = "root";
$password = "";

$conn = mysqli_connect($servername, $username, $password);

// Check connection
if (!$conn)
{
    die("Connection failed: " . mysqli_connect_error());
}
else
{
	//mysqli_query($conn, "CREATE USER 'test'@'localhost' IDENTIFIED BY 'test';");
	//mysqli_query($conn, "GRANT ALL ON db1.* TO 'test'@'localhost';");
	//mysqli_query($conn, "FLUSH PRIVILEGES");
	mysqli_query($conn, "CREATE DATABASE IF NOT EXISTS clothes");
	mysqli_query($conn, "USE clothes");
	mysqli_query($conn, "CREATE TABLE IF NOT EXISTS orders (
	`ID` int(11) unsigned NOT NULL auto_increment,
	`title` varchar(255) NOT NULL default '',
    `first_name` varchar(255) NOT NULL default '',
    `last_name` varchar(255) NOT NULL default '',
	`age` int(11) NOT NULL default 0,
	`address` varchar(255) NOT NULL default '',
	`ordering` varchar(10) NOT NULL default '',
	`size` varchar(4) NOT NULL default '',
	`price` varchar(8) NOT NULL default '',
    PRIMARY KEY  (`ID`))");
	
	$firstName = $_POST['first_name'];
	$lastName  = $_POST['last_name'];
	$title = $_POST['title'];
	$age = $_POST['age'];
	$address = $_POST['address'];
	$ordering = $_POST['ordering'];
	$size = $_POST['size'];
	$price = $_POST['price'];
	
	unset($_SESSION['query_results']);
	unset($_SESSION["modify_results"]);
	
	if(!empty($firstName) && !empty($lastName) && !empty($age) && !empty($ordering) && !empty($title) && !empty($address)&& !empty($size) && !empty($price))
	{
		$success = mysqli_query($conn, "INSERT INTO orders (title, first_name, last_name, age, address, ordering, size, price)
		VALUES ('".$title."','".$firstName."','".$lastName."','".$age."','".$address."','".$ordering."','".$size."','".$price."')");
		$_SESSION["recent_member"] = "Most Recent Order for Customer: ".$title." ".$firstName." ".$lastName;
	}
	else
	{
		$_SESSION["recent_member"] = "Invalid Data submitted for Customer: ".$title." ".$firstName." ".$lastName.". Cannot ADD.";
		
	}
		session_write_close();
		header( 'Location: clothesHomepage.php' );
		exit;
}
?>