<?php 
session_start();

$servername = "localhost";
$username = "root";
$password = "";

$conn = mysqli_connect($servername, $username, $password);

// Check connection
if (!$conn)
{
    die("Connection failed: " . mysqli_connect_error());
}
else
{
	$editMode = false;
	$deleteMode = false;
	$_SESSION["modify_results"] = '';
	if(isset($_POST))
	{
		mysqli_query($conn, "USE gym");
		foreach($_POST as $key => $invElement)
		{
			if(strpos($key, 'deleteOption')!== false && $invElement!=0)
			{
				$removeKeys[] = $invElement;
			}
			
			if(strpos($key, 'modifyOption')!== false)
			{
				$ss = substr($key, strpos($key, '_')+1);
				$id = substr($ss, 0, strpos($ss, '_'));
				$dbIndex = substr($ss, strpos($ss, '_')+1);
				$updateCommand = "UPDATE gym_members SET ".$dbIndex."='".$invElement."' WHERE id=".$id;
				mysqli_query($conn, $updateCommand);
				$editMode = true;
			}
		}
	
		foreach($removeKeys as $invKey)
		{
			mysqli_query($conn, "USE gym");
			mysqli_query($conn, "DELETE from gym_members where id = ".$invKey);
			$deleteMode = true;
		}
		
		if($deleteMode)
		{
			$removedKeys = implode(',',$removeKeys);
			$_SESSION["modify_results"] = 'Removed Members with IDs: '.$removedKeys;			
		}
	}
	
	if($editMode)
	{
		$_SESSION["modify_results"] = $_SESSION["modify_results"].' <br /> Updated Results as requested';	
	}
	
	unset($_SESSION['query_results']);
	unset($_SESSION['recent_member']);
	
	session_write_close();
	header( 'Location: gymHomePage.php' );
	exit;
	
}