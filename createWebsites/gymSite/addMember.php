<?php 
session_start();

$servername = "localhost";
$username = "root";
$password = "";

$conn = mysqli_connect($servername, $username, $password);

// Check connection
if (!$conn)
{
    die("Connection failed: " . mysqli_connect_error());
}
else
{
	//mysqli_query($conn, "CREATE USER 'test'@'localhost' IDENTIFIED BY 'test';");
	//mysqli_query($conn, "GRANT ALL ON db1.* TO 'test'@'localhost';");
	//mysqli_query($conn, "FLUSH PRIVILEGES");
	mysqli_query($conn, "CREATE DATABASE IF NOT EXISTS gym");
	mysqli_query($conn, "USE gym");
	mysqli_query($conn, "CREATE TABLE IF NOT EXISTS gym_members (
	`ID` int(11) unsigned NOT NULL auto_increment,
	`title` varchar(255) NOT NULL default '',
    `first_name` varchar(255) NOT NULL default '',
    `last_name` varchar(255) NOT NULL default '',
	`age` int(11) NOT NULL default 0,
	`sport` varchar(255) NOT NULL default '',
	`address` varchar(255) NOT NULL default '',
    PRIMARY KEY  (`ID`))");
	
	$firstName = $_POST['first_name'];
	$lastName  = $_POST['last_name'];
	$title = $_POST['title'];
	$age = $_POST['age'];
	$sport = $_POST['sport'];
	$address = $_POST['address'];
	
	unset($_SESSION['query_results']);
	unset($_SESSION["modify_results"]);
	
	if(!empty($firstName) && !empty($lastName) && !empty($age) && !empty($sport) && !empty($title) && !empty($address))
	{
		$success = mysqli_query($conn, "INSERT INTO gym_members (title, first_name, last_name, age, sport, address)
		VALUES ('".$title."','".$firstName."','".$lastName."','".$age."','".$sport."','".$address."')");
		$_SESSION["recent_member"] = "Most Recent Member added: ".$firstName.",".$lastName;
	}
		session_write_close();
		header( 'Location: gymHomePage.php' );
		exit;
}
?>