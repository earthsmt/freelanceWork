<?php
     session_start();
?>

<html>
<head>
	<title> Gym Membership </title>
</head>
<body>
<p id='start' align='center'> Welcome to the Gym Membership Site </p>
<hr />
<p align="center"><button type="button" onclick="addForm()" style="width:400px;height:120px;">Add New Membership</button></p><p>
<br /> 
<p align="center"><button type="button" onclick="searchForm()" style="width:400px;height:120px;">Searching Memberships</button></p>
<hr/>
 
<div id="showForm" style="display: none;">
<p id='header'>NEW membership signup:</p>
<form action="submit.php" method="post" id="formId" target="_self"> 
	<select name="title" name="title" id="title">
    <option value="Mr">Mr</option>
    <option value="Mrs">Mrs</option>
  </select><br /><br />
	First Name:<br /><input type="text" id="first_name" name="first_name" placeholder='Please enter first name'/><br/><br/>
	Surname:<br /><input type="text" id="last_name" name="last_name" placeholder='Please enter surname'"/><br/><br/>
	Age:<br /><input id="number" id="age" name="age" type="number"><br /><br />
	Sport:<br />
    <select name="sport" id="sport" name="sport">
    <option value="Basketball">Basketball</option>
    <option value="Football">Football</option>
     </select><br /><br />
	<div style="position:relative; left:200px;bottom:305px">Address: <br /><TEXTAREA NAME="address" id="address" ROWS=10 COLS=30></TEXTAREA></div>
	<input type="Submit" name="Submit" style="position:relative; bottom:205px" value='Submit'/>
</form>
<hr style="position:relative; bottom:205px"/>
</div>
<div id='previous'>
<p align = 'center' >
<?php
	if(isset($_SESSION['recent_member']))
	{
		echo $_SESSION['recent_member'];		
	}
	
	if(isset($_SESSION["query_results"]))
	{
		echo $_SESSION["query_results"];
	}
	
	if(isset($_SESSION["modify_results"]))
	{
		echo $_SESSION["modify_results"];
	}
?>
</p>
</div>
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script>
function addForm() {
	document.getElementById("showForm").style.display = "block";
	document.getElementById("previous").style.position = "relative";
	document.getElementById("previous").style.bottom = "205px";
    document.getElementById("header").innerHTML = "NEW membership signup:";
	document.getElementById("formId").action = "addMember.php";
}
function searchForm(){
	document.getElementById("showForm").style.display = "block";
	document.getElementById("previous").style.position = "relative";
	document.getElementById("previous").style.bottom = "205px";
	document.getElementById("first_name").setAttribute("onclick", "showHint(this)");
	document.getElementById("last_name").setAttribute("onclick", "showHint(this)");
	document.getElementById("address").setAttribute("onclick", "showHint(this)");
	document.getElementById("header").innerHTML = "Searching Memberships:";
	document.getElementById("formId").action = "readMembers.php";
}

function detectChange(row){
	var elementId = 'deleteOption_'.concat(row);
	var x = document.getElementById(elementId);
	x.setAttribute("value", "0");
	x.setAttribute("type", "hidden");
}

 function showHint(obj) {
	 
	if(obj.value!="")
	{
		var values = { };
		values[obj.id] = obj.value;
		values['ajaxMode'] = true;
		$.ajax({
			   url: "/echo/html/",
               type: "POST",
			   cache:'false',
               url: "readMembers.php",
               data: values,
			   async: false,
			   dataType: "html",
               success: function(data) {
				   var suggestion = $(data).html();
				   if(suggestion != null)
				   {
					   obj.value = suggestion;
				   }
               }
           });
	}
 }

</script>

</body>


