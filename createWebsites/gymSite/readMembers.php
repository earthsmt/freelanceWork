<?php 
session_start();

$servername = "localhost";
$username = "root";
$password = "";
$conn = mysqli_connect($servername, $username, $password);
// Check connection
if (!$conn)
{
    die("Connection failed: " . mysqli_connect_error());
}
else
{
	//$conn->query("CREATE USER 'test'@'localhost' IDENTIFIED BY 'test';");
	//$conn->query("GRANT ALL ON db1.* TO 'test'@'localhost';");
	//$test = $conn->query("FLUSH PRIVILEGES");
	//mysqli_query($conn, "CREATE DATABASE IF NOT EXISTS gym");
	mysqli_query($conn, "USE gym");
	
	$searchQuery = "SELECT * from gym_members";
	if(!empty($_POST))
	{
		$searchQuery = $searchQuery." where";
		foreach($_POST as $key => $value)
		{
			if(strpos($value, 'Submit')===false && !empty($value) && $key!='ajaxMode')
			{
				$ajaxName = $key;
				$searchQuery = $searchQuery." ".$key." like '%".trim($value)."%' and";	
			}	
		}
		$searchQuery = substr_replace($searchQuery, "", -4);
	}
	$results = mysqli_query($conn, $searchQuery);
	$table = 'No results from this Search query';
	
	if(!empty($results))
	{
		$formDeclare = '<form action="modifyMember.php" method="post" id="modifyForm" target="_self">';
		$header = "Results from last Search Query: ".PHP_EOL;
		$table = $formDeclare.$header."<table width='100%' border='1'>";
		$columnHeads = false;
		$resultsToQuery = false;

		while($row = $results -> fetch_row() ) {
			$table = $table."<tr>";
			$resultsToQuery = true;
			if(!$columnHeads)
			{
				$columnCount = count($row);
				$i =0;
				while($i < $columnCount)
				{
					$fieldName = mysqli_fetch_field_direct($results, $i)->name;
					if(isset($_POST['ajaxMode']) && $_POST['ajaxMode'] && $ajaxName==$fieldName)
					{
						echo "<div id='hello'>".$row[$i]."</div>";
						return;
					}
			
					if($i != 0) $knownColumns[] = $fieldName;
					$table=$table."<td>".$fieldName."</td>";
					$i++;
				}
				$columnHeads = true;
				$table = $table."<td>Delete?</td>";
				$table = $table."</tr><tr>";
			}
			
			foreach($row as $key => $invElement)
			{
				if($key != 0 && $key !=1 && $key!=5)
				{
					$table=$table."<td><input type='text' onchange='detectChange(".$row[0].")' name='modifyOption_".$row[0]."_".$knownColumns[$key-1]."' placeholder='".$invElement."' value='".$invElement."' /></td>";
				}
				elseif($key==1 || $key==5)
				{
					$defaultHeader = "<option value='".$invElement."' selected='selected'>".$invElement."</option>";
					if($key==1)
					{
						$options = array('Mr', 'Mrs');
					}
					
					if($key==5)
					{
						$options = array('Basketball', 'Football');
					}
					$optionHeader = '';
					foreach($options as $option)
					{
						if($option!=$invElement)
						{
							$optionHeader = $optionHeader."<option value='".$option."'>".$option."</option>";
						}		
					}
					$optionHeader = $optionHeader.$defaultHeader."</td>";	
					$table=$table."<td><select onchange='detectChange(".$row[0].")' name='modifyOption_".$row[0]."_".$knownColumns[$key-1]."' placeholder='".$invElement."' value='".$invElement."' />".$optionHeader;	
				}
				else{
					$table=$table."<td>".$invElement."</td>";
				}
			}
			$table=$table.'<td><input type="checkbox" id="deleteOption_'.$row[0].'" name="deleteOption_'.$row[0].'" value="'.$row[0].'"><br></td>';
			$table=$table."</tr>";
		}
		$table = $table."</table>";
		$endForm = '<input type="Submit" name="Submit" value="Modify" style="margin-left: 50%"/></form>';
		$table = $table.$endForm;

		unset($_SESSION['recent_member']);
		unset($_SESSION["modify_results"]);
		
		if(!$resultsToQuery)
		{
			$table = '<p align="center">No results found</p>';	
		}
		
		$_SESSION["query_results"] = $table;
	}
		session_write_close();
		header( 'Location: gymHomePage.php' );
		exit;
}
?>