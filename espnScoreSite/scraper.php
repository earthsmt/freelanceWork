<html>
<head>
</head>
<body>
<meta http-equiv="refresh" content="15">
<?php 
session_start(); ?>
<?php
date_default_timezone_set('UTC');
echo 'Last refresh: '.date('Y-m-d H:i:s').'<br />';
ini_set('max_execution_time', 300);
$date = isset($_POST['date']) ? str_replace('-', '',$_POST['date']) : '';
//Fix where the page should refresh every 15 seconds.
$_SESSION['date'] = !empty($date) ? $date : $_SESSION['date'];
$_SESSION['orig_date'] = isset($_POST['date']) ? $_POST['date'] : $_SESSION['orig_date'];
$date = $_SESSION['date'];
if(!empty($date))
{
	$responseHtml = scrapSite('http://www.scoresandodds.com/index.html');
	$scoressOddsContent = array();
	$query = '//div[@class="gameSection"]';
	$scoreOddsDataHtml = scrapContentFromHtml($query, $responseHtml, 'NCAA BB');
	$content['times'] = scrapContentFromHtml('//tr[@class="time"]', $scoreOddsDataHtml, false);
	$content['odd'] = scrapContentFromHtml('//tr[@class="team even"]', $scoreOddsDataHtml, 'allHtml');
	$content['even'] = scrapContentFromHtml('//tr[@class="team odd"]', $scoreOddsDataHtml, 'allHtml');
	$content = arrayLengthChecker($content);
	$currentLinesData = getCurrentLines($content);
	
	$responseHtml = scrapSite('http://www.espn.co.uk/mens-college-basketball/scoreboard/_/date/'.$date);
	$query = '//script[contains(text(),"window.espn.scoreboardData")]';
	$dataHtml = scrapContentFromHtml($query, $responseHtml, false);
	$teamData = getJsonFromEspn($dataHtml);
	$teamData = getStatsFromEspn($teamData);
	$html = generateHtml($teamData, $currentLinesData);	
	
	$responseHtml = scrapSite('http://www.espn.co.uk/mens-college-basketball/scoreboard/_/date/'.date('Ymd', strtotime($date .' +1 day')));
	$query = '//script[contains(text(),"window.espn.scoreboardData")]';
	$dataHtml = scrapContentFromHtml($query, $responseHtml, false);
	$teamData = getJsonFromEspn($dataHtml);
	$teamData = getStatsFromEspn($teamData);
	$html = generateHtml($teamData, $currentLinesData);	
}
else
{
	echo 'No Date Submitted!';
}

function getCurrentLines($content)
{
	$data = array();
	if(is_array($content))
	{
		foreach($content as $key => $invContent)
		{
			$index=0;
			if( ($key == 'even') || ($key == 'odd') )
			{
				foreach($invContent as $teamsHtml)
				{
					$team = scrapContentFromHtml('//a[contains(@href, "season")]', $teamsHtml, false);
					$currentLine = scrapContentFromHtml('//td[contains(@class, "currentline")]', $teamsHtml, false);
					if(is_array($team) && count($team) && is_array($currentLine) && count($currentLine))
					{
						$trimVer = trim($currentLine[0]);
						if(empty($trimVer)) $currentLine[0] = '-';
						$team[0] = strtolower(substr($team[0], strpos($team[0], " ")));
						$data[$index][trim($team[0])] = trim($currentLine[0]);
						$index++;
					}
				}
			}
		}
	}
	return $data;
}

function generateHtml($teamData, $currentLineData)
{
	$html = '';
	foreach($teamData as $group)
	{
		$html = '';
		$html = $html.'<table border="1" rules="all"><col width="250"><col width="50"><col width="50"><col width="50"><col width="70"><col width="50"><col width="50"><col width="50">';
		$date = !empty($group['date']) ? $group['date'] : '';
		if(strpos($date, '-') > 0 || ($date == 'Halftime')) $date = '<font color ="red">'.$date.'</font>';
		$teamOne = $group['compet'][0]['team'];
		$teamTwo = $group['compet'][1]['team'];
		$logoOne = $group['compet'][0]['logo'];
		$logoTwo = $group['compet'][1]['logo'];
		$homeAwayOne = $group['compet'][0]['homeAway'];
		$homeAwayTwo = $group['compet'][1]['homeAway'];
		$scoreOne = isset($group['compet'][0]['score']) ? $group['compet'][0]['score'] : '-';
		$scoreTwo = isset($group['compet'][1]['score']) ? $group['compet'][1]['score'] : '-';
		
		$inlineOneOne = isset($group['compet'][0]['inlineOne']) ? $group['compet'][0]['inlineOne'] : '-';
		$inlineOneTwo = isset($group['compet'][0]['inlineTwo']) ? $group['compet'][0]['inlineTwo'] : '-';
		
		$inlineTwoOne = isset($group['compet'][1]['inlineOne']) ? $group['compet'][1]['inlineOne'] : '-';
		$inlineTwoTwo = isset($group['compet'][1]['inlineTwo']) ? $group['compet'][1]['inlineTwo'] : '-';
		foreach($currentLineData as $key => $invLineData)
		{
			$lineOne = array_key_exists(strtolower($teamOne), $invLineData) ? $invLineData[strtolower($teamOne)] : '-';
			if($lineOne != '-')
			{
				$key = array_keys(array_diff_key($invLineData, [strtolower($teamOne) => ""]))[0];
				$lineTwo = $invLineData[$key];
				break;
			}
			$lineTwo = array_key_exists(strtolower($teamTwo), $invLineData) ? $invLineData[strtolower($teamTwo)] : '-';
			if($lineTwo != '-')
			{
				$key = array_keys(array_diff_key($invLineData, [strtolower($teamTwo) => ""]))[0];
				$lineOne = $invLineData[$key];
				break;
			}
		}
		//Feature which we are not allowed to display lines with no score.
		if( ($lineOne == '-') && ($lineTwo=='-') )
		{
			continue;
		}
		$fgOne = isset($group['stats'][0]) ? scrapContentFromHtml('//td[@class="fg"]', $group['stats'][0], false) : false;
		$fgTwo = isset($group['stats'][1]) ? scrapContentFromHtml('//td[@class="fg"]', $group['stats'][1], false) : false;
		$fgOne = !empty($fgOne) ? substr($fgOne[0], strpos($fgOne[0], '-')+1) : '-';
		$fgTwo = !empty($fgTwo) ? substr($fgTwo[0], strpos($fgTwo[0], '-')+1) : '-';
		$threePtOne = isset($group['stats'][0]) ? scrapContentFromHtml('//td[@class="3pt"]', $group['stats'][0], false) : false;
		$threePtTwo = isset($group['stats'][1]) ? scrapContentFromHtml('//td[@class="3pt"]', $group['stats'][1], false) : false;
		$threePtOne = !empty($threePtOne) ? substr($threePtOne[0], strpos($threePtOne[0], '-')+1) : '-';
		$threePtTwo = !empty($threePtTwo) ? substr($threePtTwo[0], strpos($threePtTwo[0], '-')+1) : '-';
		$totalOne = (is_numeric($fgOne) && is_numeric($threePtOne)) ? $fgOne + $threePtOne : '-';
		$totalTwo = (is_numeric($fgTwo) && is_numeric($threePtTwo)) ? $fgTwo + $threePtTwo : '-';
		$ftOne = isset($group['stats'][0]) ? scrapContentFromHtml('//td[@class="ft"]', $group['stats'][0], false) : false;
		$ftTwo = isset($group['stats'][1]) ? scrapContentFromHtml('//td[@class="ft"]', $group['stats'][1], false) : false;
		$ftOne = !empty($ftOne) ? substr($ftOne[0], strpos($ftOne[0], '-')+1) : '-';
		$ftTwo = !empty($ftTwo) ? substr($ftTwo[0], strpos($ftTwo[0], '-')+1) : '-';
		$orbOne = isset($group['stats'][0]) ? scrapContentFromHtml('//td[@class="oreb"]', $group['stats'][0], false) : '-';
		$orbTwo = isset($group['stats'][1])? scrapContentFromHtml('//td[@class="oreb"]', $group['stats'][1], false) : '-';
		$toOne = isset($group['stats'][0]) ? scrapContentFromHtml('//td[@class="to"]', $group['stats'][0], false) : '-';
		$toTwo = isset($group['stats'][1]) ? scrapContentFromHtml('//td[@class="to"]', $group['stats'][1], false) : '-';
		$html = $html.'<tr><td><div style="display: inline-block;width: 150px"><b>'.$date.'</b></div><div style="display: inline-block;width: 20px">1</div><div style="display: inline-block;width: 40px">2</div><div style="display: inline-block;width: 10px"><font face="verdana" size="4">T</font></div></td><td align="center"><b>LINE</b></td><td align="center"><b>FG</b></td><td align="center"><b>3PT</b></td><td align="center"><b>TOTAL</b></td><td align="center"><b>ft</b></td><td align="center"><b>ORB</b></td><td align="center"><b>to</b></td></tr>';
		$html = $html.'<tr><td> <img src="'.$logoOne.'" alt="Logo Img 1" style="width:50px;height:50px;vertical-align:middle;"><div style="display: inline-block;width: 100px;vertical-align: middle;"><p><font face="verdana" size="4">'.$teamOne.'</font></p><p><font face="verdana" size="1">'.$homeAwayOne.'</font></p></div><div style="display: inline-block;width: 20px;vertical-align:middle;"><font face="verdana" size="2">'.$inlineOneOne.'</font></div><div style="display: inline-block;width: 40px;vertical-align:middle;"><font face="verdana" size="2">'.$inlineOneTwo.'</font></div><div style="display: inline-block;width: 10px;vertical-align:middle;"><font face="verdana" size="4">'.$scoreOne.'</font></div></td><td align="center"><font face="verdana" size="2">'.$lineOne.'</font></td><td align="center"><font face="verdana" size="2">'.$fgOne.'</font></td><td align="center"><font face="verdana" size="2">'.$threePtOne.'</font></td><td align="center"><font face="verdana" size="2">'.$totalOne.'</font></td><td align="center"><font face="verdana" size="2">'.$ftOne.'</font></td><td align="center"><font face="verdana" size="2">'.$orbOne[0].'</font></td><td align="center"><font face="verdana" size="2">'.$toOne[0].'</font></td></tr>';
		$html = $html.'<tr><td> <img src="'.$logoTwo.'" alt="Logo Img 2" style="width:50px;height:50px;vertical-align:middle;"><div style="display: inline-block;width: 100px;vertical-align: middle;"><p><font face="verdana" size="4">'.$teamTwo.'</font></p><p><font face="verdana" size="1">'.$homeAwayTwo.'</font></p></div><div style="display: inline-block;width: 20px;vertical-align:middle;"><font face="verdana" size="2">'.$inlineTwoOne.'</font></div><div style="display: inline-block;width: 40px;vertical-align:middle;"><font face="verdana" size="2">'.$inlineTwoTwo.'</font></div><div style="display: inline-block;width: 10px;vertical-align:middle;"><font face="verdana" size="4">'.$scoreTwo.'</font></div></td><td align="center"><font face="verdana" size="2">'.$lineTwo.'</font></td><td align="center"><font face="verdana" size="2">'.$fgTwo.'</font></td><td align="center"><font face="verdana" size="2">'.$threePtTwo.'</font></td><td align="center"><font face="verdana" size="2">'.$totalTwo.'</font></td><td align="center"><font face="verdana" size="2">'.$ftTwo.'</font></td><td align="center"><font face="verdana" size="2">'.$orbTwo[0].'</font></td><td align="center"><font face="verdana" size="2">'.$toTwo[0].'</font></td></tr>';
		$html = $html.'</table><br /><br />';
		echo($html);
	}
	return $html;
}

function getStatsFromEspn($teamData)
{
	if(is_array($teamData))
	{
		$urls = array();
		foreach($teamData as $invData)
		{
			array_push($urls, $invData['link']);
		}
		$multiHtml = multiRequest($urls);
		foreach($teamData as $key => &$invData)
		{
			$output = scrapContentFromHtml('//tr[td = "TEAM"]', $multiHtml[$key], 'allHtml');
			$invData['stats'] = $output;
		}
	}
	return $teamData;	
}

function multiRequest($data, $options = array()) {
 
  // array of curl handles
  $curly = array();
  // data to be returned
  $result = array();
 
  // multi handle
  $mh = curl_multi_init();
 
  // loop through $data and create curl handles
  // then add them to the multi-handle
  foreach ($data as $id => $d) {
 
    $curly[$id] = curl_init();
 
    $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
    curl_setopt($curly[$id], CURLOPT_URL,            $url);
    curl_setopt($curly[$id], CURLOPT_HEADER,         0);
    curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);
 
    // post?
    if (is_array($d)) {
      if (!empty($d['post'])) {
        curl_setopt($curly[$id], CURLOPT_POST,       1);
        curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
      }
    }
 
    // extra options?
    if (!empty($options)) {
      curl_setopt_array($curly[$id], $options);
    }
 
    curl_multi_add_handle($mh, $curly[$id]);
  }
 
  // execute the handles
  $running = null;
  do {
    curl_multi_exec($mh, $running);
  } while($running > 0);
 
 
  // get content and remove handles
  foreach($curly as $id => $c) {
    $result[$id] = curl_multi_getcontent($c);
    curl_multi_remove_handle($mh, $c);
  }
 
  // all done
  curl_multi_close($mh);
 
  return $result;
}

function scrapSite($url)
{
	if(is_string($url))
	{
		$ch = curl_init($url);
		$userAgent = 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0';
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept-Language: pt-BR;q=0.8,en-US;q=0.6,en;q=0.4"));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_ENCODING,  '');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_ENCODING, '');
		//curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		curl_setopt( $ch, CURLOPT_USERAGENT, $userAgent );
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
	return false;
}

function scrapContentFromHtml($query, $html, $partHtml)
{
	if(!empty($html))
	{
		$dom = new DOMDocument();
		libxml_use_internal_errors(true);
		$dom->loadHTML($html);
		$xpath = new DOMXpath($dom);
		if(!$partHtml)
		{
			$row = $xpath->query($query);
			foreach($row as $value)
			{
				$values[] = trim($value->textContent);
			}
		}
		else
		{
			$childNode = $xpath->evaluate($query);
			foreach ($childNode as $child) 
			{ 
				$innerHTML = $child->ownerDocument->saveHtml($child);
				if(($partHtml != "allHtml") && strpos($innerHTML, $partHtml) !== false)
				{
					$values = $innerHTML;
					return $values;
				}
				elseif($partHtml == "allHtml")
				{
					$values[] = $innerHTML;
				}
			}
		}
		return !isset($values) ? false : $values;
	}
	return false;
}

function arrayLengthChecker($arrays)
{
	foreach($arrays as $key => $array)
	{
		$length = !isset($length) ? count($array) : $length;
		if($length != count($array))
		{
			return false;
		}
	}
	return $arrays;
}

function getJsonFromEspn($dataHtml)
{
	$begin = 'window.espn.scoreboardData 	= ';
	$end = ';window.espn.scoreboardSettings =';
	$teamData = array();
	if(count($dataHtml) == 1)
	{
		$json = substr($dataHtml[0], strpos($dataHtml[0], $begin) + strlen($begin));
		$json = substr($json, 0, strpos($json, $end));
		$jsonDecode = json_decode($json, true);
		$c=0;
		foreach($jsonDecode['events'] as $key => $event)
		{
			$compets = $event['competitions'][0]["competitors"];
			$linkInfo = $event['links'][0]['href'];
			$statusEvent = isset($event['status']['type']['name']) ? $event['status']['type']['name'] : false;
			$date = isset($event['date']) ? $event['date'] : false;
			$dateStored = $date;
			if(date($_SESSION['orig_date']) != date('Y-m-d', strtotime( $dateStored ) - ( 60 * 60 * 6))) continue;
			if($statusEvent != 'STATUS_SCHEDULED')
			{
				$date = isset($event['status']['type']['detail']) ? $event['status']['type']['detail'] : $date;
			}
			preg_match('/(gameId=([\d]+))/', $linkInfo, $matches);
			$link = 'http://www.espn.com/mens-college-basketball/boxscore?gameId='.$matches[2];
			$teamData[$c] = array();
			$teamData[$c]['link'] = $link;
			$teamData[$c]['date'] = extractHeaderInformation($date, $statusEvent);
			$teamData[$c]['status'] = $statusEvent;
			
			foreach(array_reverse($compets) as $index => $compet)
			{
				$team  = $compet['team']["shortDisplayName"];
				$logo =  isset($compet['team']["logo"]) ? $compet['team']["logo"] : '';
				$score = $compet['score'];
				$records = isset($compet['records']) ? $compet['records'] : '';
				$homeInfo = isset($records[0]['summary']) ? $records[0]['summary'] : false;
				$awayInfo = isset($records[2]['summary']) ? $records[2]['summary'] : false;
				$homeAwayInfo = ($homeInfo!==false && $awayInfo!==false) ? '('.$homeInfo.', '.$awayInfo.' away)' : '';
				$inlineScoreOne = isset($compet['linescores'][0]['value']) ? $compet['linescores'][0]['value'] : false;
				$inlineScoreTwo = isset($compet['linescores'][1]['value']) ? $compet['linescores'][1]['value']  : false;
				$teamData[$c]['compet'][] = array('team' => $team, 'homeAway' => $homeAwayInfo, 'logo'=> $logo, 'score' => $score, 'inlineOne'=>$inlineScoreOne, 'inlineTwo'=>$inlineScoreTwo);
			}
			$c++;
		}
	}
	return isset($teamData) ? $teamData : false;
}

function extractHeaderInformation($data, $status)
{
	if($data == false || $status == false)
	{
		return false;
	}
	if($status == 'STATUS_SCHEDULED')
	{
		if( (strpos($data, 'T') > 0) && (substr($data, -1) == 'Z') )
		{
			$data = substr($data, strpos($data, 'T')+1);
			$data = substr($data, 0, strpos($data, 'Z'));
			$data = date( 'g:i A', strtotime( $data ) - ( 60 * 60 * 6));
			$data = $data. ' CST';
		}
	}
	elseif($status == 'STATUS_FINAL')
	{
		$data = 'FINAL';
	}
	elseif($status == 'STATUS_IN_PROGRESS')
	{
		$data = trim(substr($data, 0, strpos($data, 'Half')));
	}
	return $data;
}
?>
</body>
</html>