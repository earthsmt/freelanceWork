<?php

function scrapFromSite($url)
{
	$ch = curl_init($url);
	$userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0';
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept-Language: en-US;en;q=0.5"));
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_ENCODING,  '');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_ENCODING, '');
	//curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
	curl_setopt( $ch, CURLOPT_USERAGENT, $userAgent);
	$response = curl_exec($ch);
	curl_close($ch);
	return $response;
}

function scrapContentFromHtmlPage($html,$query)
{
	if($html)
	{
		$dom = new DOMDocument();
		libxml_use_internal_errors(true);
		$dom->loadHTML($html);
		$xpath = new DOMXpath($dom);
		$childNode = $xpath->evaluate($query);
		foreach ($childNode as $child) 
		{
			$innerHtml = $child->ownerDocument->saveHtml($child);
			$values = $innerHtml;
		}
		return $values;
	}
	
	return false;
}

function removeHtmlElement($openTag, $html)
{
	//Format: <tag element=''>
	$tag = trim(substr($openTag, 2, strpos($openTag, ' ')-1));
	$removeTag = $openTag.'[\n . \- \w \r\n\r\n r\r n\n]*\<\/'.$tag.'\>';
	$html = preg_replace('/'.$removeTag.'/', '', $html);
	return $html;
}
?>