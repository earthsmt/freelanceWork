<html>
<head>
<meta http-equiv="Refresh" content="60">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<title>
Betting Information
</title>
</head>
<body>
<script type='text/javascript'>

function createCookie(name, value) {
    var expires = "";
    if (true) {
        var date = new Date();
        date.setTime(date.getTime() + (1*60*24*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(cname)
{
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function reloadCurrentPage(obj, dateChange)
{
	currentHref = obj.getAttribute('hrefAlt');
	
	if(dateChange>0)
	{
		var currentDate = getCookie('currentDate');
		if(currentDate)
		{
			var date = new Date(currentDate);
		}
		else
		{
			var date = new Date();
		}
		if(dateChange==1)
		{
			date.setDate(date.getDate() - 1);
		}
		else
		{
			date.setDate(date.getDate() + 1);
		}
		var dd = date.getDate();
		var mm = date.getMonth()+1; //January is 0!
		var yyyy = date.getFullYear();

		if(dd<10) {
			dd = '0'+dd
		} 

		if(mm<10) {
			mm = '0'+mm
		}
		index = currentHref.indexOf(".html");
		if(index>0)
		{
			currentHref = currentHref.slice(0, -13);
		}
		currentHref = currentHref + yyyy + mm + dd + ".html";
		cookieValue =  yyyy + '-' + mm + '-' + dd;
		createCookie('currentDate', cookieValue);
	}
	else
	{
		createCookie('currentDate', '');
	}

	createCookie('currentPage', currentHref);
	window.location.reload();	
}

      function getOddsPageOther(obj, pageNum) {
          var toKeep = new Array();
          $('#oddsHolder').find('td.bookColumn', 'th.bookColumn').each(function(index,t) {
              if (t.getAttribute('rel') == 'page' + pageNum)
                  toKeep.push(t);
              t.style.display = "none";			  
          });
          toKeep.forEach(function(t) {
			  t.style.display = '';
          });
		  $('#oddsHolder').find('th.bookColumn').each(function(index,t) {
              if (t.getAttribute('rel') == 'page' + pageNum)
                  toKeep.push(t);
              t.style.display = "none"; 
          });
          toKeep.forEach(function(t) {
			  t.style.display = '';
          });
          var divContainers = $('#oddsHolder').find('div.odds_pages');
          divContainers.each(function(index, container) { 
				var children = container.getElementsByTagName('a');
				for (let a of children)
				{
					a.setAttribute("class", "");
					if(a.innerHTML==obj.innerHTML)
					{
						a.setAttribute('class','pageCurrent');
					}	
                 }
          });
		refresher.options.currentPage=parseInt(obj.innerHTML);	
      } 

</script>

<?php

$currentPage = isset($_COOKIE["currentPage"]) && !empty($_COOKIE["currentPage"]) ? $_COOKIE["currentPage"] : 'http://www.donbest.com/all/odds/';

include('./utilityFunctions.php');

$output = scrapFromSite($currentPage);
$output = '<link href="http://www.donbest.com/style.css?v=2" type="text/css" rel="stylesheet">'.scrapContentFromHtmlPage($output, "//div[@id='col1_content']");
$output = removeHtmlElement('\<h1 class\=\"titleBar titleBarScoreOdds\"\>', $output);
$output = str_replace('href="/', 'onclick="reloadCurrentPage(this);" hrefAlt="http://www.donbest.com/', $output);

$output = str_replace('<a href="#" onclick="return getDay(-1);">', '<a hrefAlt="'.$currentPage.'" onclick="reloadCurrentPage(this, 1);">', $output);
$output = str_replace('<a href="#" onclick="return getDay(1);">', '<a hrefAlt="'.$currentPage.'" onclick="reloadCurrentPage(this, 2);">', $output);
$output = str_replace('getOddsPage(this', 'getOddsPageOther(this', $output);

$output = '<div class="section module DBScoresOddsClass Modulenothing" id="module2_1">'.$output.'</div>';
$output = str_replace('Reload this page', '',$output);

print($output);

?>

</body>