<?php
require("./utilityFunctions.php");

$stats = array();
$cookies = "Cookie: __auc=b245bc921686f4dbd787ce980ae; __cfduid=deb2c9fcad7ab5b2abdeff19ae93fc0ab1553587852; app-session=l9kv23lna5at0eecdqddnsmf91; cdmgeo=gb; euconsent=BOeArHCOeArHCABABAENABAAAAAAAA; __asc=eb17d3fb169cf3b7c42fa58538b";
$fileName = "historicStatsForAccounts.csv";

$mode = "w+";
$headersRequired = true;
if(file_exists($fileName))
{
  $mode = "a+";
  $headersRequired = false;
}

if(file_exists("accountsToMonitor.csv"))
{
	$accounts = getAccountsToMonitor();
	if($accounts)
  {
    $profiles = array_map(function($item) { return $item["profile"];}, $accounts);
  }
}

$seasonsToMonitor = (array) range(1, 14, 1);

foreach($profiles as $profile)
{
  foreach($seasonsToMonitor as $season)
  {
    print "Scrapping from profile account: ".$profile." for season ".$season.PHP_EOL;
    $url = "https://masteroverwatch.com/profile/pc/global/".$profile."/season/".$season;
    $output = scrapeUrl($url, $cookies);
    if($output)
    {
     	$overallStats = scrapeContentFromHtml("//div[contains(@class, 'stats-list-box')]", $output, true);
    	if($overallStats)
    	{
        $stats[$season]["Profile"] = $profile;
        $stats[$season]["Season"] = $season;
        $stats[$season]["Time Scrapped"] = date("Y-m-d H:i:s");
        $recordStat = true;
    		foreach($overallStats as $invStat)
    		{
    			$label  = scrapeContentFromHtml("//span[contains(@class, 'stats-label')]", $invStat, false);
    			$value = scrapeContentFromHtml("//strong[contains(@class, 'stats-value')]", $invStat, false);
    			if($label && $value && count($label)==1 && count($value)==1)
    			{
            $stat = implode($value);
            if(!$stat){
              $recordStat = false;
            }
    				$stats[$season][implode($label)] = implode($value);
    			}
    		}
        if(!$recordStat)
        {
          unset($stats[$season]);
        }
        else
        {
          $headers = array_keys($stats[$season]);
        }
    	}
    }
  }

  $stats = array_values($stats);

  if($headers){
    if($headersRequired)
    {
      writeResultsToCsvFile(array($headers), $fileName ,$mode);
      $headersRequired = false;
      $mode = "a+";
    }
    writeResultsToCsvFile($stats, $fileName ,$mode);
    $headers = false;
  }
}

?>
