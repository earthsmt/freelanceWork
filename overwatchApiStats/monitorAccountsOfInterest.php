<?php

require("./utilityFunctions.php");
if(file_exists("accountsToMonitor.csv"))
{
	$nicknameProfiles = getAccountsToMonitor();
	print("Getting Overwatch Stats for: ".count($nicknameProfiles)." accounts".PHP_EOL);
	$headersNotNeeded = false;
	$file = "results.csv";
	if(file_exists($file))
	{
		$headersNotNeeded = true;
	}
	foreach($nicknameProfiles as $index => $nicknameProfile)
	{
		$nickname = array($nicknameProfile['nickname']);
		$profile  = array($nicknameProfile['profile']);
		print("Scraping stats from account: ".$nicknameProfile['profile']." ( ".$index."/".count($nicknameProfiles)." )".PHP_EOL);
		getStatsForAccountsAssociatedtoNickname($nickname, $profile, $file, "a+", $headersNotNeeded);
		$headersNotNeeded = true;
	}
}
else
{
	print("No accounts to monitor".PHP_EOL);
}

?>
