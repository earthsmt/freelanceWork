<?php

function scrapeUrl($url, $cookies=false)
{
	$handle = curl_init();
	$userAgent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0';
	curl_setopt($handle, CURLOPT_URL, $url); // Set the result output to be a string.
	curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($handle, CURLOPT_USERAGENT, $userAgent);
	if($cookies)
	{
		curl_setopt($handle, CURLOPT_HTTPHEADER, [$cookies]);
	}
	$output = curl_exec($handle);
	curl_close($handle);
	return $output;
}


function multiRequest($data, $options = array()) {
	  // array of curl handles
	  $curly = array();
	  // data to be returned
	  $result = array();

	  // multi handle
	  $mh = curl_multi_init();

	  // loop through $data and create curl handles
	  // then add them to the multi-handle
	  foreach ($data as $id => $d) {

	    $curly[$id] = curl_init();

	    $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : 'http://overwatchy.com/profile/pc/global/'.$d;
	    curl_setopt($curly[$id], CURLOPT_URL,            $url);
	    curl_setopt($curly[$id], CURLOPT_HEADER,         0);
	    curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);

	    // post?
	    if (is_array($d)) {
	      if (!empty($d['post'])) {
		curl_setopt($curly[$id], CURLOPT_POST,       1);
		curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
	      }
	    }

	    // extra options?
	    if (!empty($options)) {
	      curl_setopt_array($curly[$id], $options);
	    }

	    curl_multi_add_handle($mh, $curly[$id]);
	  }

	  // execute the handles
	  $running = null;
	  do {
	    curl_multi_exec($mh, $running);
	  } while($running > 0);


	  // get content and remove handles
	  foreach($curly as $id => $c) {
	    $result[$id] = curl_multi_getcontent($c);
	    curl_multi_remove_handle($mh, $c);
	  }

	  // all done
	  curl_multi_close($mh);

	  return $result;
}

function scrapeContentFromHtml($query, $html, $partHtml)
{
	if(!empty($html))
	{
		$dom = new DOMDocument();
		libxml_use_internal_errors(true);
		$dom->loadHTML($html);
		$xpath = new DOMXpath($dom);
		if(!$partHtml)
		{
			$row = $xpath->query($query);
			foreach($row as $value)
			{
				$values[] = trim($value->textContent);
			}
		}
		else
		{
			$childNode = $xpath->evaluate($query);
			foreach ($childNode as $child)
			{
				$innerHTML = $child->ownerDocument->saveHtml($child);
				if(($partHtml != "allHtml") && strpos($innerHTML, $partHtml) !== false)
				{
					$values = $innerHTML;
					return $values;
				}
				elseif($partHtml == "allHtml")
				{
					$values[] = $innerHTML;
				}
			}
		}
		return !isset($values) ? false : $values;
	}
	return false;
}

function getStatsFromOverwatchyServerForAccountName($account)
{
	$url = 'http://overwatchy.com/profile/pc/global/'.$account;
	return scrapeUrl($url);
}

function getUsersToScrape()
{
	$nickNames = array();
	$fileHandle = fopen("overwatchNicknames.csv", "r");

	while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE)
	{
		$nickNames[] = $row[0];
	}
	unset($nickNames[0]);
	return $nickNames;
}

function getAccountsToMonitor()
{
	$accounts = array();
	$fileHandle = fopen("accountsToMonitor.csv", "r");

	while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE)
	{
		$accounts[] = array("profile" => $row[1], "nickname" => $row[2]);
	}
	unset($accounts[0]);
	return $accounts;
}

function writeResultsToCsvFile($stats, $fileToWriteTo, $mode='w+')
{
	if(!empty($stats))
	{
		$fp = fopen($fileToWriteTo, $mode);
		foreach($stats as $fields) {
		    fputcsv($fp, $fields);
		}
		fclose($fp);
	}
}

function extractProfile(&$profile) {
   $profile = substr($profile, strrpos($profile, '/')+1);
}

function getStatsForAccountsAssociatedtoNickname($nicknames, $knownProfiles = array(), $fileToWriteTo='accountsToMonitor.csv', $mode='w+', $keyHeaders=false)
{
	$statsData = array();
	$headers =  array('date_scrapped', 'profile', 'username', 'level', "portrait", "endorsement-sportsmanship-value", "endorsement-sportsmanship-rate",
				"endorsement-shotcaller-value", "endorsement-shotcaller-rate", "endorsement-teammate-value",
				"endorsement-teammate-rate", "endorsement-level", "endorsement-frame", "endorsement-icon",
				"games-quickplay-won", "games-competitive-won", "games-competitive-lost", "games-competitive-draw",
				"games-competitive-played", "games-competitive-win_rate", "competitive-rank", "competitive-rank_img",
				"levelFrame", "star");
	foreach($nicknames as $nickname)
	{
		$statsData[$nickname] = array();
		$html = true;
		if(!$knownProfiles)
		{
			$html = scrapeUrl("https://overwatchtracker.com/search?name=".$nickname);
		}
		if($html)
		{
			if(!$knownProfiles)
			{
				$profiles = scrapeContentFromHtml("//a[contains(@href,'profile')]/@href", $html, false);
				if($profiles)
				{
					print($nickname." has (".count($profiles).") associated profiles.".PHP_EOL);
				}
			}
			else
			{
				$profiles = $knownProfiles;
			}
			if($profiles)
			{
				if(!$knownProfiles)
				{
					array_walk($profiles, "extractProfile");
				}
				$start = 0;
				$len = 2;
				$collectedProfiles = array_slice($profiles, $start, $len);
				$results = array();
				do
				{
					if(!$knownProfiles)
					{
						time_nanosleep(0, 500000000) === true;
					}
					$results = array_merge($results, multiRequest($collectedProfiles));
					$start = $start + $len;
					$collectedProfiles = array_slice($profiles, $start, $len);

				}while(!empty($collectedProfiles));

				foreach($results as $profileIndex => $result)
				{
					$jsonDecodedStats = json_decode($result, true);
					if($jsonDecodedStats)
					{
						if(isset($jsonDecodedStats['competitive']['rank']) && !empty($jsonDecodedStats['competitive']['rank']))
						{
							$singleArrayStats = array();

							foreach($jsonDecodedStats as $index => $element)
							{
								if(!is_array($element))
								{
									$singleArrayStats[$index] = $element;
								}
								else
								{
									foreach($element as $levelOneKey => $lvlOneElement)
									{
										if(!is_array($lvlOneElement))
										{
											$singleArrayStats[$index.'-'.$levelOneKey] = $lvlOneElement;
										}
										else
										{
											foreach($lvlOneElement as $levelTwoKey => $lvlTwoElement)
											{
												if(!is_array($lvlTwoElement))
												{
													$singleArrayStats[$index.'-'.$levelOneKey.'-'.$levelTwoKey] = $lvlTwoElement;
												}
											}
										}
									}
								}


							}
							if(!$keyHeaders)
							{
								$statsData[$nickname][] = $headers;
								$keyHeaders = true;
							}
							$singleArrayStatsForCsv = array();
							foreach($headers as $header)
							{
								if($header=='date_scrapped')
								{
									$singleArrayStatsForCsv[$header] = date("Y-m-d H:i:s");
								}
								elseif($header=='profile')
								{
									$singleArrayStatsForCsv['profile'] = $profiles[$profileIndex];
								}
								elseif(isset($singleArrayStats[$header]))
								{
									$singleArrayStatsForCsv[$header] = $singleArrayStats[$header];
								}
								else
								{
									$singleArrayStatsForCsv[$header] = '-';
								}

							}
							$statsData[$nickname][] = $singleArrayStatsForCsv;
						}
					}
				};
				writeResultsToCsvFile($statsData[$nickname], $fileToWriteTo, $mode);
				$mode = 'a+';
			}
		}
	}
}

?>
