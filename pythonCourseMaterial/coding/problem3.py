# Problem 3.
# Build a scraper that will scrape PDF/ZIP files from hosted from open source domain, Filebin.

#External packages to use for program.
#Package: requests - used to extract HTML from websites
import requests
#Package: re - used for regex pattern matching for strings
import re
#Package: urllib.request - used for downloading content from given website into a local file
import urllib.request
#Package: zipfile - unzip a local zip file
import zipfile

#Method which scrapes HTML from a given website using requests package.
def rawCurlRequestFromWebsite(website):
    try:
	#response is an object which holds HTML behind a website, request headers and status codes etc...
        response = requests.get(website)
	# Code: 200 - implies the requests.get command was successful in scraping HTML from the website
        if response.status_code==200:
	    #return the HTML presented on the website
            return response.text
        else:
	    #return False to indicate failure (has code status != 200)
            return False
    except:
            return False

#Method which downloads content from a website and stores the downloaded content in a file called, filename
def downloadFile(website, filename):
    #Make a request to  get content behind a given URL.
    response = urllib.request.urlopen(website)
    #Open, store contents and close the  chosen file.
    file = open(filename, 'wb')
    file.write(response.read())
    file.close()

#Method to extract the download link from the given HTML provided (by looking for a specific regex pattern)
def getDownloadLinkFromHtml(html):
    if html:
        #Look for the following pattern in the HTML: center><a href="https://filebin.net/ebasdhjrlksz7zen/pythonCourseMaterial.pdf?t=pjc0zg0i" class
        links = re.findall('center\"\>\<a href\=\"(https?\:\/\/[\w \d \s \: \- \' \! \? \_ \& \= \\\\ \. \/]+)\" class', html)
	#Find the first occurance of that pattern and return
        return links[0]
    else:
	#If no HTML was given, do not bother doing the regex search
        return False

#Method to unzip a given file which is in ZIP format
def unzipFile(file):
    #If file is presented, then proceed
    if file:
	#Open [file].zip fie in read format and extract contents into file, [file]
        zip_ref = zipfile.ZipFile(file, 'r')
        zip_ref.extractall('.')
        zip_ref.close()

#Execution of code..

# Download a given PDF file and store locally as: document.pdf
html = rawCurlRequestFromWebsite('https://filebin.net/ebasdhjrlksz7zen/pythonCourseMaterial.pdf')
link = getDownloadLinkFromHtml(html)
downloadFile(link, "document.pdf")

# Download a given ZIP file, unzip that file and store locally as: document.zip
html = rawCurlRequestFromWebsite('https://filebin.net/kxz66szxgxw9u0hs/document.zip')
link = getDownloadLinkFromHtml(html)
downloadFile(link, "document.zip")
unzipFile("document.zip")
