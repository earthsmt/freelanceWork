# Problem 2.
# Build a scraper that will scrape the contents from your recent history videos from www.youtube.com

#Conversion of HTTP to Python Call requests: https://curl.trillworks.com/

#External packages to use for program.
import requests
#Package: re - used for regex pattern matching for strings
import re
#Package: csv - used for writing contents to and from a CSV file
import csv

#Method which scrapes from youtube.com using the requested headers and cookies in the cURL request.
def rawCurlRequestFromYoutube():
    cookies = {
        'VISITOR_INFO1_LIVE': 'wO6UbfSkNpc',
        'PREF': 'al=en-GB&f1=50000000',
        'CONSENT': 'YES+GB.en-GB+V9',
        's_gl': 'e3ea30620a3c9e68880a30237b86c6cfcwIAAABHQg==',
        'YSC': 'HowmeoUKbA4',
        'GPS': '1',
        'SID': 'egdsfH59XweiD-YgRWUU0kGEZP7YuNT6xBEtf8yN-LpqWPsw_iARORK6CCs29Iz3rLrISg.',
        'HSID': 'AgVq6wZxoSeSI2gyA',
        'SSID': 'AGBJPNS_PlOI2i6Gi',
        'APISID': 'PiG8D0ZuqBP2T7FM/AHD-pCtyKWvBbpTlP',
        'SAPISID': 'dWuFGtfycDgZ-UEF/A7UwUmcf1UZVI8wQC',
        'LOGIN_INFO': 'AFmmF2swRQIhAI6h1MLUozVoJaku5AAlh6PGi2oU65MkUiYWwD7FFPu6AiBPS1LG7qQvKQoZPLMaGZcX2HyCIz_YPXy26wTu8nzQaA:QUQ3MjNmeE1MQ2drcC1iUkQtcVphcjczWjJBVGdSeVFlcHpTZ2NRdXdLdlNBdzBzSDFXaV9uZFRpU045TDc4Z2dTSzUzbTQtV1JyZXpfNFdwWDNZZFRSVl9zek5TNGRyb0UtbWU1dnJRMWNxcDl5ZW5OTHFnSndsc1Fxb3c3ZkZkeG96WHNWTm5HTHQtUWdrU2puOTFlcVdOaWxjVURpYTVvNlBleVBUejRxSmJWQVktRldBWHJ3',
        'SIDCC': 'AN0-TYsFJGpbfTpq0IkcTgpGNJcuecgC69cx2_Seq54herWp5P5T929RYAxf8thj2a6cDIVd',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'Connection': 'keep-alive',
        'DNT': '1',
        'Host': 'www.youtube.com',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0',
    }

    try:
        #response is an object which holds HTML behind a website, request headers and status codes etc...
        response = requests.get(website, headers=headers, cookies=cookies)
        # Code: 200 - implies the requests.get command was successful in scraping HTML from the website
        if response.status_code==200:
            #return the HTML presented on the website
            return response.text
        else:
            #return False to indicate failure (has code status != 200)
            return False
    except:
            return False
#Find the links and the title of the videos via regex pattern matching
def regexFindTitleAndLinks(html):
    # If HTML is provided, then attempt to find the links and title
    if html:
        links = re.findall('\"url\"\:\"\/(watch\?v\=[\w \d \s \: \- \' \! \? \_ \| \" \* \&  \. \+ \& \= \\\\ \,]+)\"\,\"webPageType', html)
        titles = re.findall('\}\}\,\"simpleText\"\:\"(?!\d\d?\:\d\d)([\w \d \s \: \- \" \*  \. \& \+ \' \! \? \_ \| \& \= \\\\ \,]+)', html)
        # Links obtained will have format: /watch?v=[video_id]. Each of those need to be converted to: http://www.youtube.co.uk/watch?v=[video_id]
        links = cleanLinks(links)
        #For each link and video title, append the result to a CSV file.
        for index, link in enumerate(links):
            row = [];
            row.append(link)
            row.append(titles[index])
            writeRowsToCsvFile(row)
    else:
        return False
#Clean URLs, so the format of the links are: http://www.youtube.co.uk/watch?v=[video_id]
def cleanLinks(links):
    data = []
    for link in links:
        data.append('http://www.youtube.co.uk/'+str(link));
    return data

#Write results to a CSV file, where a single row is written.
def writeRowsToCsvFile(row):
    #Open file, results.csv in APPEND mode (so new data is appended to the file)
    with open('results.csv', 'a+') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(row)


#Execution of code..
website = 'https://www.youtube.com/feed/history'
#Perform a curl request to get content behind the website
html = rawCurlRequestFromYoutube()
#If HTML can be obtained, then attempt to regex the link and channel meta from the HTML and
#send results to CSV file.
if html:
    print('Success obtaining HTML from '+website)
    regexFindTitleAndLinks(html)
else:
    #If no success, then report no results can be scraped.
    print('No results could be scraped from '+website)
