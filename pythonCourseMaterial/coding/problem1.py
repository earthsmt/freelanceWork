# Problem 1.
# Build a scraper that will scrape sitemaps from Wikipedia hosted on Pastebin.

#External packages to use for program.
#Package: requests - used to extract HTML from websites
import requests
#Package: re - used for regex pattern matching for strings
import re
#Package bs4, BeautifulSoup - used to parse HTML, so HTML elements can be called directly
from bs4 import BeautifulSoup
#Used to decode HTML characters &gt; to > etc
from html.parser import HTMLParser

#Method which scrapes a sitemap from a given website using requests package.
def rawCurlRequestFromWebsite(website):
    try:
        #response is an object which holds HTML behind a website, request headers and status codes etc...
        response = requests.get(website)
        # Code: 200 - implies the requests.get command was successful in scraping HTML from the website
        if response.status_code==200:
             #return the HTML presented on the website
            return response.text
        else:
            #return False to indicate failure (has code status != 200)
            return False
    except:
            return False
#Parse HTML through BeautifulSoup and call HTML elements directly.
def parseHtml(html, search):
    #Only parse, HTML has been submitted
    if html:
        #Parse the HTML, through the HTML parser
        parsedHtml = BeautifulSoup(html, features="html.parser")
        #Call elements directly in the code - for example to collect all <loc> tags - parsedHtml.find_all("loc")
        elements = parsedHtml.find_all(search)
        #Get the links within the <loc>[link]</loc> via regex pattern matching, to only collect the [link]
        list = []
        for item in elements:
            #Regex find [link] within the <loc> tag
            links = re.findall('\>(https?\:\/\/[\w \d \s \: \- \' \! \? \_ \, \& \= \\\\ \. \/ \( \) \%]+)\<', str(item))
            #Append the first regex match [link] into the array of links.
            list.append(links[0])
        return list


#Execution of code..

# Scrape Wikipedia sitemap, parse HTML and print the links.
html = rawCurlRequestFromWebsite('https://pastebin.com/AEawsbDf')
htmlParser = HTMLParser()
links = parseHtml(htmlParser.unescape(html), 'loc')
print(links)
