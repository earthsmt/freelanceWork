<?php

function scrapFromSite($url)
{
	$ch = curl_init($url);
	$userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:58.0) Gecko/20100101 Firefox/58.0';
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept-Language: en-US;en;q=0.5"));
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_ENCODING,  '');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_ENCODING, '');
	//curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
	curl_setopt( $ch, CURLOPT_USERAGENT, $userAgent);
	$response = curl_exec($ch);
	curl_close($ch);
	return $response;
}

function scrapContentFromHtml($query, $html, $partHtml)
{
	if(!empty($html))
	{
		$dom = new DOMDocument();
		libxml_use_internal_errors(true);
		$dom->loadHTML($html);
		$xpath = new DOMXpath($dom);
		if(!$partHtml)
		{
			$row = $xpath->query($query);
			foreach($row as $value)
			{
				$values[] = trim($value->textContent);
			}
		}
		else
		{
			$childNode = $xpath->evaluate($query);
			foreach ($childNode as $child) 
			{ 
				$innerHTML = $child->ownerDocument->saveHtml($child);
				if(($partHtml != "allHtml") && strpos($innerHTML, $partHtml) !== false)
				{
					$values = $innerHTML;
					return $values;
				}
				elseif($partHtml == "allHtml")
				{
					$values[] = $innerHTML;
				}
			}
		}
		return !isset($values) ? false : $values;
	}
	return false;
}

function scrapContentFromHtmlPage($html,$query)
{
	if($html)
	{
		$dom = new DOMDocument();
		libxml_use_internal_errors(true);
		$dom->loadHTML($html);
		$xpath = new DOMXpath($dom);
		$childNode = $xpath->evaluate($query);
		foreach ($childNode as $child) 
		{
			$innerHtml = $child->ownerDocument->saveHtml($child);
			$values = $innerHtml;
		}
		return $values;
	}
	
	return false;
}

function removeHtmlElement($openTag, $html)
{
	//Format: <tag element=''>
	$tag = trim(substr($openTag, 2, strpos($openTag, ' ')-1));
	$removeTag = $openTag.'[\n . \- \w \r\n\r\n r\r n\n]*\<\/'.$tag.'\>';
	$html = preg_replace('/'.$removeTag.'/', '', $html);
	return $html;
}

function generateSearchUrl($url, $loginSessionCookie, $searchTerm)
{
	$searchUrl =  'curl --silent "'.$url.'" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0" -H "Accept: */*" -H "Accept-Language: en-GB,en;q=0.5" --compressed -H "Referer: https://www.nsn-now.com/search/search.aspx" -H "X-Requested-With: XMLHttpRequest" -H "X-MicrosoftAjax: Delta=true" -H "Cache-Control: no-cache" -H "Content-Type: application/x-www-form-urlencoded; charset=utf-8" '.$loginSessionCookie.' -H "DNT: 1" -H "Connection: keep-alive"';

	if($searchTerm)
	{
		$searchUrl = $searchUrl.' --data "ctl00"%"24RadScriptManager1=ctl00"%"24BodyContent"%"24ctl00"%"24BodyContent"%"24searchPanelPanel"%"7Cctl00"%"24BodyContent"%"24RadButton1&__EVENTTARGET=&__EVENTARGUMENT=&ctl00_RadScriptManager1_TSM="%"3B"%"3BSystem.Web.Extensions"%"2C"%"20Version"%"3D4.0.0.0"%"2C"%"20Culture"%"3Dneutral"%"2C"%"20PublicKeyToken"%"3D31bf3856ad364e35"%"3Aen-US"%"3Ac9cbdec3-c810-4e87-846c-fb25a7c08002"%"3Aea597d4b"%"3Ab25378d2"%"3BTelerik.Web.UI"%"2C"%"20Version"%"3D2016.1.225.40"%"2C"%"20Culture"%"3Dneutral"%"2C"%"20PublicKeyToken"%"3D121fae78165ba3d4"%"3Aen-US"%"3Aab312d5a-8d8b-4310-a879-b25aeb34b5c7"%"3A16e4e7cd"%"3Aed16cbdc"%"3Af7645509"%"3A88144a7a"%"3A24ee1bba"%"3Ae330518b"%"3A2003d0b8"%"3Ac128760b"%"3A1e771326"%"3Ac8618e41"%"3A1a73651d"%"3A333f8d94"%"3A8e6f0d33"%"3A1f3a7489"%"3A4877f69a"%"3A92fe8ea0"%"3Afa31b949"%"3A19620875"%"3A874f8ea2"%"3Af46195d3"%"3A490a9d4e"%"3Abd8f85e4"%"3A86526ba7"%"3A6d43f6d9&ctl00_SiteHeader_RadStyleSheetManager2_TSSM="%"3BTelerik.Web.BlueMetroCustom"%"3Aen-US"%"3A020199cd-fcc5-44b2-95f8-e09064411781"%"3A426ab14c"%"3Acac06f1a"%"3BTelerik.Web.UI"%"2C"%"20Version"%"3D2016.1.225.40"%"2C"%"20Culture"%"3Dneutral"%"2C"%"20PublicKeyToken"%"3D121fae78165ba3d4"%"3Aen-US"%"3Aab312d5a-8d8b-4310-a879-b25aeb34b5c7"%"3A45085116"%"3Aed2942d4"%"3A8cee9284"%"3A3e0dfe6c"%"3Ae7750fd8"%"3Ad7e35272"%"3Aef4a543"%"3Afe53831e"%"3BTelerik.Web.UI.Skins"%"2C"%"20Version"%"3D2016.1.225.40"%"2C"%"20Culture"%"3Dneutral"%"2C"%"20PublicKeyToken"%"3D121fae78165ba3d4"%"3Aen-US"%"3A3241164d-ce18-449b-b3d6-fbfa5f3cd52c"%"3Ab5804a7d"%"3A5588224d"%"3Aa01bf17&__VIEWSTATE=ULl4ACVvsuRH3IK"%"2Bhm"%"2FLfgCRO"%"2BBi9TzrxRBi1Uc4Vq77RFYuNYg1YLeOMjQSaRVs2"%"2BMrYwhTxk2c"%"2FFouAJLlH1i3WE2J7DKpnxC5xD4Acj6qJ"%"2F8EDbsybCXIEOEmfMawlpjW"%"2Bw0edBzCPksBKSGPz4g1xh94oH1rJkS3nEJq7KFRS5IFC"%"2FNM1QXdMwjgC3adZKu6v0lLlcQ"%"2Bsc5PTtsQP7F"%"2FvzWvcKA6dbXZoAM383oiaxWb4RDgxkyAgd6Asr7dt51CkyESJf9tkx4Kp2fj"%"2BspzPek6iCmyz0NPDeVywdsUmjUXt"%"2BOtg7AP5UpndtAGFOH3o1WU4bBh2vtiSGkCsuahNnQ4x"%"2BxJAWongjns7ZnWjvjChx1U73T1TUB5N3GZ00ANIhpoaBpg58GM8He8gCbrjc9JpqlRa"%"2BfZmMSLhoTEnzU"%"2BKY05MG4cXeE"%"2Bs7aIZdbvguhPpHKiRqP9ALdzyLrZOz09WYXyYhbsn3YAKYvKaUkTe67YhR3gYoJ777D8YMhLcgFRcXYH8N6Njc2P"%"2FOcplYbcXI2KfUnWdqAldOBqKkaRslDCwcnHAX22b9"%"2B4FhVEBXJsOwcdQN"%"2FlJ"%"2BI79U0Jbry6U"%"2Fk4NWVI4QaK1SqdBF3KCXQLSbskpwPtRvXQlo"%"2BQGHWw9XQHGBz0OXxxmxUukkLrH3ofVOCfGC6AlkyBdK7jjk3YokCg89TG3xugHjf2KZfjf6I5lLB5Nm44wf7bow5PiBKvYg"%"2FFvleRrWy1VgEgOF4klBGsr"%"2FH8c9"%"2FOXIy2wALRevPyMBG48"%"2Fv1AXLUwHD0P6cUDzrq"%"2Ff7bfIsTdnNL8hmepE5iSIVpipA6Moa3YPA1YqkbyGS29MDauKJRtoemz3a8r63pHFwAIUSQtGLJk"%"2Fp5x1Jd6J2FQoOkw8oEZk7ldtwTOf6tzSuQH8tNFUzlyJnxUfNtCJN8Kw"%"2Fm7ollt5D8iaWBeBIKA9jC7"%"2F5ew8Jmvw9O2ItAg7MyZgVh9dc1A0XtEZyyKXoVE5nT7jgvk7QhxSZtgG8a"%"2BcfbUCkvSOnpVEjnnz0LUDM8d"%"2FjKq"%"2FCoc"%"2FCbYmizv"%"2BYTPMuQc4KimmOnu4yKnCs"%"2B"%"2FaBDhmU49vA1JE6eHAvge4FnZfwYEQxctXKrrm1LYYeD1lG"%"2Fbw0cFjQVXpmGEaPBcuYPIKvt8gU0fUQkt30CgGWhdzfAG4YDwz5"%"2FVy87u6Cr8pWJp6QMKKf17KWfa5Ey8bvWON"%"2BQ1jkUwS3eBBiNwRukki9khMMnq9KR2Vc69mgZMOXzY6PpQpNRiyZOQqmkaV7Y"%"2BWvZ5U8T76"%"2BB3"%"2Forzde10oA"%"2FQ5B8abzD0DV9gyK4dIbs6jn2kqeQlATYeC7u2qRJZaE0LqNmfS6MJ991eKNFVxE5iYjuo1jkwILBFeJ"%"2BpHX84QwGHJ"%"2FQ8pm2jLcg79C6COljC0KogP4y"%"2FxZpW"%"2BxzU86Wz"%"2F7bfwhAGbr7e1bebh2p"%"2Bp5lm8xhL47k7RqXN7jNQjJ7F"%"2FLzH"%"2B4x8eGYXoRNBpeSgVWEjeM1G3qrz"%"2FfCuHppAYf1d42ymTL4mFoovUrptzMJDi9r7lfDbom6IGv3XvcI"%"2BMXLEX72S0QkO9mTVCwERcecfe4L8sHRTluLtfeGAO7zD36rczliqWqBqEuHarrIBQGPI2R3ZLBQorZJE4oy"%"2FWRAnQEqk9pVeFS4jQtyILDCe3cCam6NE2ondZaWmizzXq"%"2F5G56lXvXOezyeAEVt6cjJSdoRT"%"2B8VRSnJjcmtVA"%"2FUgtl"%"2FU9zdG"%"2FkanB4MZqZ3B5aGcWgyKHb"%"2Bu2vig"%"2FX2EUXBgAvBPqnqfjofeilrXz8xGj8NZknCqv5XZSpyoH30OLOAaVUxJ58HLeYLG9yyZ0V4GQXvbwX6Kq9WGS8ERMNK8JX6YrnzaKeEcg4A2HKd3olyq62M17UYZvqAW3pt"%"2F90yERA2bg1c"%"2Bw2IVi"%"2Fl0GlpQh4"%"2FPN1zDhzdltDU7L8ILWZnQwHVtRfrJ7DUDtKUYlukjJ1"%"2FEO27pnZTLtqxHq1tre8worGZsILXaJ"%"2FjtqLRuLQoe1g46z7gaeQomhCTcuuhxNfDZs89jZWnf5hvszDMO9"%"2B1wK3N5L8pO1zJ0iEP1GQ9KpMLtA5N3"%"2B88IZibrWX"%"2BGBRIU"%"2FD0bKzA1z9Oal7jlxUcOgKZoBc1XZWwWX750Lmot"%"2FWh2cgduYkeLk7NYDXEqWv8Efuig5Zd8FHGa"%"2BwEv"%"2B54rsfClst"%"2F2ro8dq2lZYOLZ8UdrivxK0n7wiYmFUpiUxZyjkNJvf9CgLf9mr67tOH7eYDh"%"2B3Hv0u88MbZrwGHZ4wrao"%"2FC"%"2FpvXoqdVYp"%"2BqWjOiPe7JpPOWdFnfBWSHKeK"%"2FUrZV0hvRKnALrg6vnFqAUC9sdNSHS4XYWjffTLM8KP252lLYOyInozsNrYN3hrxL1I0uNWh"%"2B6uwEwY9BhBwEJ2n2TlnS9"%"2FpS2VLZ0oNGbDOXDFyS0Llwj19DJowuC0m2lUn8nkaZB"%"2B4FPHLxh4KE9ssz"%"2FThHX67mfrNVxQZ7RhvmwCagWyYYXYZs5aKX1uHAMBQgcmEt1GjZhb8zXpag8Hw0402lsIq3KTf"%"2BWn7o081CK3ejm1fMAH8"%"2BBfYtlUlPoO6czGTsn6prC5kCZZlm6nhbD4Na1"%"2FKxCI9mBNHgW5WskhccHtpZsMxdQtQM"%"2BKWWb8A9PtaVGgA695UdPv6hcrguON45YVw9E7Bsg63gHatVr3nPl"%"2BIurBb8L3Y"%"2B7"%"2BP"%"2Bzzjiq4PHFoRFOm0KtXXz"%"2Bycfp"%"2FqzDKjLbDnOe61YTQ4Ab4PSOko0hHI9q6bzOQvzT1DrOAj87JMKZkPHwNytSw4liQveq4fD3eF"%"2FRS5JVr6HAeh"%"2F1x2Px"%"2B00rkD1MnNYSEeHR6m9zErW9QUZFNfxRYNPCZFEga1tOfVrNo8pTrt"%"2BKANBjLnnz"%"2FFpQoT"%"2FNOqO6ZY9jgEY9"%"2Bb7e0NYuNo3QDoI5vOSL0k5QF2eblN"%"2B8AtxCj3dUJ"%"2FaAd"%"2BeWI3Wxfb7mX"%"2F7B1HrbMMVyoKQU9UcbWuwtkShjaWc3mjc"%"2BSf9ISAbkCitJOvG03WAkgQYKFrqByEa4YLA1LIaoN1MwvX237oCkes1GI3KTJ"%"2Fu6SlQGy3aguiimTK2AuZkQW4fl9xwqzmcBaN"%"2FaPHx"%"2BY1yKs1TT"%"2FtjsJbstYG0o5UfiS2"%"2BI1BykcjnRCCMXBMukkYhL8lESWo"%"2FyXzYzMzYe8dM"%"2F31Sx1bVwwKDVNxbsW0W62xV5VsDqXl"%"2FrXrCHm1yo3pBID5h"%"2FH"%"2FDF2U"%"2FC26QO2jzojn23b8NZBhv"%"2FXm71Npcr28x1qDFXmMKwRjHGvOY5H1I"%"2FDEkBJwNvLpvBMvcKkhCdqNXWkkZEDnZQhd0QVmNm1MraESVAg4CWusw8crx"%"2F2WAw1W8lcTjtsL7OBXq5lO6xY6sx5r0UxLgjLOrWH2Mepw68HyLEIiQ5H1zothGJXlJn7BxuszJuOajfeg"%"2BrO0axtcGC7HplqkRbNSZ95zqXXd44q0j9ueNtkBcko8tbCU6SL3Lv2eiLSDcIFC0kqfVJkRhVDSFIKkm"%"2FLEZN"%"2B0hsf"%"2FVUVU10peqjeebmycF"%"2B7XhKXWt37xJ6LaGztA"%"2B1ExrSJqIA5b5HwuDcyFqJ3tUkqNytFI8z"%"2BG1dgL1hOSbHrOQACFFQzA2XPjsptWc0w7FKarjJiQbmk615"%"2FdP09iz6QKIJJT3myPQOw1haO7I9otLauXdOJ9abFTR729sagK75ezm9r4o7nUQAu4oiMnoD7ZuQShghHkadIT2w3fpIMMgZuf4Hc4C"%"2FS3azb2uFdUJAtY0xSfrUvV750w"%"2B4AXds6pe7yiItTtuIXQEpxNfaJTe"%"2BFSVAWEW"%"2BB9PcBJd2PUmajapiJZuLxJLa4WrZR8lWr8WCcM0LtTMSDTLBb9pbKDadI41oe3nBTTjTRO9ZKRj5CKVBh8701QstSdEdnLxHivNoT6cqkNNioivMDYDaOAS1vBoMkEoBF43ZMjsZoaf7fxzaiAUBhpl0pUK7YuHaPK94uTUMXXxkLCTIF2tpobTgdDL4P8h7"%"2BbOGBth1iSEr2iCMs3"%"2BKu0dHtlcAdLX7t4NMBu23uVGHiCliwXb"%"2Fg"%"2BE5tt57mEOmy2D"%"2F2D"%"2BJXDC6Xgc9Qf8u8r98xrVqMBMzc8oj"%"2BR"%"2BWxd"%"2BBvwpiGyKfB8dCzDqMBQANF04ztFVhjNO9w7Kl0RuLw1n"%"2FeWv8dJQmtvTEqTnDUyvfmAW6Fb"%"2BlQvmKBru8JypfnYVqJw2kS5FgcCEjeLT1h"%"2BhS18Elw4nJkkHtTovVWX9SmoL7DDVCt5DDnWMRqecUWBUohvWuVxE"%"2FvKtqr9rTEO1uofeFxIXrP3eZqSkKzh1Ll2tcUSGmNDeGRmTte2BplOxl5GS388Rh3d1JKa"%"2Bi3IuJPV2bwnPlyNFKzg9JgEvFfcaTfRIcty"%"2FAI74G9iqrAHTd"%"2BwQJOYFfBnRpHnTSN1K79lGdqOH3vkIPe3dEMEdiIZdIJUH7rjpHxyzqSwE54i69v8G2qpBlqIBBgt4eHifqlUyYHlsDMBq6PTN2z6"%"2FGXutk3wgAObkiqMfv5Vd7bsOYcSpL2xp6QLq0vZ6xw3GHy54T6543ig7G1BsU8TaH9MNPvTgxrooTVOGFmxpkQ"%"2FqSJPq"%"2F4rswyseXw2N1i90zMcQdlqXswxjctxJ7WKycqMQhk0HE4"%"2F2eesF9aGv9S"%"2F6QkHSDZ1Ou1"%"2FaHEtlBZ8XZPFakSejiArFpxnGheYCCgp"%"2FsYivJ"%"2BG"%"2BLdcUbZYb8r4C5n0TTFH0vi3viX"%"2FJnjUjoWttjMwppvUSYlvqrsIyTV16w"%"2FNnODPSeOUchzZhoC5FIgNsg5FeYizqqGuRZ9wK23KwnQvzeQZUDzEwylUtLS3UdbpJUQSEAdfnw79pNWvX7UmUzthTWH3xCgnMZwHmewcGGca2dx6Ms53wzf2NPx0l5D75UoLzprCzS9fmWN2VJ00yPaEkI6WcliIZ2KhhBub1v0vSEStC9o1a398Cwq76aoG3BXQ4IZ"%"2FbBKQnwu4tsrss"%"2BrlZDZq4BT1a3LvlG9f"%"2FBIgJedohOfiEVN5JEmxuZ66"%"2ByaQwAXeC"%"2F7UsjfQaSKLTMvrbpPfkP9seTmC"%"2FO7qOHeEeWOckxd32hXNrW4GiSR3"%"2BaD"%"2F"%"2FIPsIU8aUDXSzAkuLLl9RC7v3QSu2c1sW7UHAxp53zGltJRZJdfLFVJUcM0HYv1O"%"2BdroOsjN2WEnhaoOrvxLiaLQTtMH8lKB95C3iqQLdGoTKgy5KsaNdpm"%"2BcbDdAeaf"%"2Bbai3p"%"2BrAnWFFD7441d3aRrUjSIX504JFN"%"2F4dbzvEdWvy"%"2F0JfBH"%"2FNjV4UL4xF"%"2F3FKLLH8Buij6Dsdk"%"2BXuI4gc0rdwQPIwoTjAhDJuy5NsQJsMBtnJP407xTS30CzY"%"2BBqW2r"%"2BXpk9Bf0ZrQUMruhF05B"%"2FqDU1CFnMMANtZt0hN2banWR5plFWK"%"2FnWVP"%"2B"%"2FE5X"%"2BVtTTbXnz0Jhv1EysAhq29Iu9gWJbrNwnKBTU0EwbgXyOO0ajMH6AEc2ZtMGWMeW"%"2B7QvpvzipSF4FWdw0eQ2KhsXGVwQnZoJbsy1Qivy8cMubmM6J21aH7EMsGI5b9Ms2W2A2J9bf3Mpgp6PPY4mrKlO5gOgj0HMGARHmMBsYcZTQ6c5WBcnyzfskYZSX7u"%"2FXcjBkepCGALlcZO6N"%"2FJPNFdvrN07RK"%"2Fd5YLjK1aU0Uv0c2kBJP5gx9Ns80VJHSPdZagIrqi2Qrm34aRb3OMO4GpCoRowF0lBFAHV2tZhx6nZcNPOvssi0di6BhJ6LDaEyL9WYJRTZx4bU7W1hPAnpSGTiuGeciTJ7lGQulwpuya62CPUF8uwFrufXv0EbM"%"2FbiqONSmII2BoCgVMeFWJZ"%"2FQ7V0WaXU2Sq954k67xTxDeKCF6WMibk"%"2BYzr34VT"%"2FWz"%"2BVliB43zLHT42H5uB2N6ir6ZW1jFIxdgM7eogxsLkktc"%"2FJ3Pu5kSgqdtE84fFRko6nDXhf"%"2FEAgtGIxpPhpplj3SRtOvTegL4CqNVteLeEws"%"2BSSXysF"%"2BIL9nXIccXhMe7zjBIf5yhderVHQD0dn3iWaAulV"%"2B22tkbLhFDTHAC1KH8YT7bHt2Qa3krhHE5bJNZU6owj9LNDHyGb"%"2BprBJpjgH6lAxojU9scKFC6oN9if"%"2FVTbGG1qacZLfJMr6kkCw9j2zkW111kAeDYCGcn2hl3yzloA"%"2BN76v"%"2B1OwmzSiHRi64Gb8ttIXjpf8h"%"2BxqTucQw13O9jcQ2Bhm63c4"%"2BQcIKeedz6bDW9dwY1UijmtvfNs382lu06Ika6EcH1"%"2BlHz"%"2FW5w1SPaQ5QLdTE8NzrfV6bk0gFFwoL0rFc87MVQBlK2WU25pomKz"%"2B"%"2B1BFAV5"%"2FRU74zDuAdaxDnWD0MG8m3KsnuE5M0eDM6J2dFPIstKkIhUazOG"%"2BkYE4pbPKyQhKRQQI9pJ6rBSfDigPhRGkszuGjPF0nTyCGPOlkus"%"2BItYhtHz2BtEjULI4X7LwURHzJ6ouyBbFOlmhrcGXr51Kt8KpSSsEFyZR48JpmSbdCUblDENPwYJu3j"%"2FbQCz2ELVh6ypdTtmxRqLHUzSuoYMIJV9aMPywzDHX3ifPKr"%"2B98vZOrT2932ZqhGUlA2s"%"2FrSkQp7QtZTrenpYJO"%"2F3wmZb1UDdax2U7rMmA4zccHLrY"%"2F"%"2BLNzTO2AxY8qt59ylINrtEGw03mF4BoKCMJpnmFUXbPPQDjgAvtzAkfAOy9k2hHu9ehJokvKNYtc4fJRewmydJI9V8uVZL4uNszKJYfJ3Mgk"%"2BHNdfeEosr4KJpZas4RAaMmX7rpUF6UnpzJY1nf3YlvoK4KAimkPPe8B45phvrZrOUNRKzrYKAGjMxZdH"%"2Bta1cr8AObXbgXpe1EqENieELePhKllLoKLmsc"%"2FEGfOm2x1nkrRLCHlE7z"%"2BnenDsfHpsSCTNuLfPiEXiL3cP7b32qywXlsHD0zzgaElgMlNoJGFJ"%"2F"%"2BzOnlGSsGBqNnaRUPvN5tFWTGBvTK7v4OUIYdW5bnMmfL"%"2BMOmsJcnSwKNOPayIyEmpHgy2"%"2B7K0svOH6V5ONxnYlqkksW6Tme"%"2Bd0VOkn"%"2BvZcWMiaFW3uqp7uvQ"%"2FRMTCCTeL2k9gjU"%"2FoPWX"%"2BLyQqQhdOmMqq7EEbzi30E0lo5Dch0tMbDGs2ZPSrL7FTdIZElkd0G0OiQvDdt2s6F9hVW"%"2FF"%"2FtXeGdu"%"2BWXoNn"%"2BTfC2CkwhUakkhNmv6SWuw9hmTtgoy0WcBfWv6xyM"%"3D&__VIEWSTATEENCRYPTED=&ctl00_SiteHeader_RadMenu2_ClientState=&ctl00_aTABS_RadTabStrip1_ClientState="%"7B"%"22selectedIndexes"%"22"%"3A"%"5B"%"5D"%"2C"%"22logEntries"%"22"%"3A"%"5B"%"5D"%"2C"%"22scrollState"%"22"%"3A"%"7B"%"7D"%"7D&ctl00_BodyContent_RadWindow1_ClientState=&ctl00_BodyContent_TechSearchWINDOW_ClientState=&ctl00_BodyContent_CageDisplay_ClientState=&ctl00_BodyContent_RadWindowManager1_ClientState=&ctl00_BodyContent_RadFormDecoratorWeb_ClientState=&ctl00"%"24BodyContent"%"24tboxNSN=&ctl00"%"24BodyContent"%"24tboxPart='.$searchTerm.'&ctl00"%"24BodyContent"%"24tboxFSC=&ctl00"%"24BodyContent"%"24tboxDescript=&ctl00"%"24BodyContent"%"24tboxMNSN=&ctl00"%"24BodyContent"%"24tboxCage=&ctl00"%"24BodyContent"%"24tboxCompany=&ctl00"%"24BodyContent"%"24tboxSOL=&ctl00"%"24BodyContent"%"24tboxPRYP=&ctl00"%"24BodyContent"%"24tboxAWD=&ctl00_BodyContent_RadButton1_ClientState="%"7B"%"22text"%"22"%"3A"%"22Search"%"22"%"2C"%"22value"%"22"%"3A"%"22"%"22"%"2C"%"22checked"%"22"%"3Afalse"%"2C"%"22target"%"22"%"3A"%"22"%"22"%"2C"%"22navigateUrl"%"22"%"3A"%"22"%"22"%"2C"%"22commandName"%"22"%"3A"%"22search"%"22"%"2C"%"22commandArgument"%"22"%"3A"%"22"%"22"%"2C"%"22autoPostBack"%"22"%"3Atrue"%"2C"%"22selectedToggleStateIndex"%"22"%"3A0"%"2C"%"22validationGroup"%"22"%"3Anull"%"2C"%"22readOnly"%"22"%"3Afalse"%"2C"%"22primary"%"22"%"3Afalse"%"2C"%"22enabled"%"22"%"3Atrue"%"7D&ctl00_BodyContent_RadButton2_ClientState="%"7B"%"22text"%"22"%"3A"%"22Refresh"%"22"%"2C"%"22value"%"22"%"3A"%"22"%"22"%"2C"%"22checked"%"22"%"3Afalse"%"2C"%"22target"%"22"%"3A"%"22"%"22"%"2C"%"22navigateUrl"%"22"%"3A"%"22"%"22"%"2C"%"22commandName"%"22"%"3A"%"22clear"%"22"%"2C"%"22commandArgument"%"22"%"3A"%"22"%"22"%"2C"%"22autoPostBack"%"22"%"3Atrue"%"2C"%"22selectedToggleStateIndex"%"22"%"3A0"%"2C"%"22validationGroup"%"22"%"3Anull"%"2C"%"22readOnly"%"22"%"3Afalse"%"2C"%"22primary"%"22"%"3Afalse"%"2C"%"22enabled"%"22"%"3Atrue"%"7D&ctl00_BodyContent_RadButton3_ClientState="%"7B"%"22text"%"22"%"3A"%"22Help"%"22"%"2C"%"22value"%"22"%"3A"%"22"%"22"%"2C"%"22checked"%"22"%"3Afalse"%"2C"%"22target"%"22"%"3A"%"22"%"22"%"2C"%"22navigateUrl"%"22"%"3A"%"22"%"22"%"2C"%"22commandName"%"22"%"3A"%"22"%"22"%"2C"%"22commandArgument"%"22"%"3A"%"22"%"22"%"2C"%"22autoPostBack"%"22"%"3Afalse"%"2C"%"22selectedToggleStateIndex"%"22"%"3A0"%"2C"%"22validationGroup"%"22"%"3Anull"%"2C"%"22readOnly"%"22"%"3Afalse"%"2C"%"22primary"%"22"%"3Afalse"%"2C"%"22enabled"%"22"%"3Atrue"%"7D&ctl00"%"24BodyContent"%"24tboxSSPNumber=&ctl00"%"24BodyContent"%"24tboxNTOEqNumber=&ctl00"%"24BodyContent"%"24tboxNTONumber=&ctl00"%"24BodyContent"%"24qstr=&ctl00"%"24BodyContent"%"24nSearch=&ctl00"%"24BodyContent"%"24tboxTSearch_Criteria=&ctl00"%"24BodyContent"%"24tboxTSearch_Display=&ctl00"%"24BodyContent"%"24tboxFCompany=&ctl00"%"24nsnDownloadFile_part2_LINK=&__ASYNCPOST=true&ctl00"%"24BodyContent"%"24RadButton1=Search&RadAJAXControlID=ctl00_BodyContent_RadAjaxManager1"';
	}

	return $searchUrl;
}

function extractCookie($searchCurl)
{
	if(strpos($searchCurl, "Cookie")!==false)
	{
		$searchCurl = substr($searchCurl, strpos($searchCurl, "Cookie"));
		$searchCurl = substr($searchCurl, 0, strpos($searchCurl, "' -H"));
		return $searchCurl;
	}
	return false;
}
?>
