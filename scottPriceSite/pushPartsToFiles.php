<?php

include('./utilityFunctions.php');

$parts = array_map('str_getcsv', file('./parts.csv'));

$fileNo = 1;
$noInFiles = 5000;
$counter =1;

$partSender = './parts/sendPart.csv';

foreach($parts as $part)
{
	$fileName = './parts/parts-'.$fileNo.'.csv';
	$fp = ($counter==1) ? fopen($fileName, 'w+') : fopen($fileName, 'a+');
	fputcsv($fp, $part);
	fclose($fp);
	$counter++;
	if($counter >$noInFiles)
	{
		$cmd = 'nohup php priceScraper.php '.$fileNo.' > /dev/null 2>&1 &';
		exec($cmd);
		$fileNo++;
		$counter = 1;
	}
}




?>
