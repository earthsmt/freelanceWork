<?php

include('./utilityFunctions.php');

$fileNo = trim($argv[1]);

$csvFile = file('./parts/parts-'.$fileNo.'.csv');
$writeResults = fopen('./results/results-'.$fileNo.'.csv','a+');
$searchCurl = file_get_contents('./searchCurl.txt');
$cookie = extractCookie($searchCurl);
if(!$cookie)
{
	print('Cookie could not be found. Please update'.PHP_EOL);
	die();
}
print('Cookie Found. Starting scraper.'.PHP_EOL);
$headersNeeded = true;

foreach ($csvFile as $line)
{
	$parts = explode(',', trim($line));
	$cmd = "sed -i 1d ./parts/parts-".$fileNo.".csv";
	exec($cmd);
	$searchTerm = $parts[0];
	$loginSessionCookie = '-H "'.$cookie.'"';

	$url = 'https://www.nsn-now.com/search/search.aspx';
	$searchUrl = generateSearchUrl($url, $loginSessionCookie, $searchTerm);
	$output = shell_exec($searchUrl);
	print('Scrapping part: '.$searchTerm.PHP_EOL);

	if(strpos($output, '16|pageRedirect')!==false)
	{
		print('Scrap URL needs to be updated'.PHP_EOL);
		die();
	}
	else
	{
		if(strpos($output, 'pageRedirect')!==false)
		{
			print('Direct part found. Scraping prices.'.PHP_EOL);
			$nsns = array('bypass');
		}
		else
		{
			$nsns = scrapContentFromHtml('//td/a[@href and contains(@href,"/detail/Summary.aspx")]', $output, true);
		}
		if($nsns)
		{
			print('Found: '.count($nsns).' NSNs'.PHP_EOL);
			foreach($nsns as &$nsn)
			{
				if(strpos($output, 'pageRedirect')===false)
				{
					$nsn = str_replace('href="', 'href="https://www.nsn-now.com', $nsn);
					$urlInfo = scrapContentFromHtml('//a/@href', $nsn, false);
					$valueInfo = scrapContentFromHtml('//a', $nsn, false);
					$url = $urlInfo[0];
					$nsn = $valueInfo[0];
					print('Extracting Proc and Management Prices from NSN: '.$nsn.PHP_EOL);
				}
				else
				{
					$decodedOutput = urldecode($output);
					$url = substr($decodedOutput, strpos($decodedOutput, '/detail/'));
					$url = rtrim('https://www.nsn-now.com'.$url, '|');
					print('Getting direct prices for: '.$searchTerm.PHP_EOL);
				}
		
				$pricesHtml = shell_exec(generateSearchUrl($url, $loginSessionCookie, ''));
				if($pricesHtml)
				{
					if($nsn=='bypass')
					{
						$nsn = scrapContentFromHtml('//span[contains(@id, "lblNSN")]', $pricesHtml, false);
						$nsn = $nsn[0];
					}
					$procPrice = scrapContentFromHtml('//span[contains(@id, "PHUPrice")]', $pricesHtml, false);
					$procPrice = $procPrice[0];
					$managementPrice = scrapContentFromHtml('//span[contains(@id, "UnitPrice")]', $pricesHtml, false);
					$managementPrice = $managementPrice[0];
					$saveData = array('part'=>$searchTerm, 'nsn'=>$nsn, 'Proc Price'=>$procPrice, 'Management Price'=>$managementPrice);
					if($headersNeeded)
					{
						$headers = array_keys($saveData);
						fputcsv($writeResults, $headers);
						$headersNeeded =false;
					}
					fputcsv($writeResults, $saveData);
				}
				
			}
		}
		else
		{
			print('No NSNs found'.PHP_EOL);
		}
	}
}
fclose($writeResults);



