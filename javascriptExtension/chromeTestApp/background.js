//background.js

/*
var output = 'Test';
fetch(chrome.runtime.getURL("myfile.html")).then(r => r.text()).then(result => {
    chrome.extension.onMessage.addListener(function(message,sender,sendResponse){
          if(message.text == "getStuff")
            sendResponse({type:result});
        });   
})
*/

async function getUserAsync()
{
  let response = await fetch(`https://phantomjscloud.com/api/browser/v2/a-demo-key-with-low-quota-per-ip-address/?request={url:%22http://cinch-test-site.s3-website-ap-southeast-2.amazonaws.com%22,renderType:%22html%22}`);
  let data = await response.text()
  return data;
}

getUserAsync()
  .then(function afterFooAll(data) { // possibly use `finally` here?
    chrome.extension.onMessage.addListener(function(message,sender,sendResponse){
          if(message.text == "getStuff")
            sendResponse({type:data});
        });
  });