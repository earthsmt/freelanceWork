from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from wit import Wit
import subprocess
import os
import json
import inflect
# Keeps a list of all items in the to-do list
todolist = []

#Listener function which will execute if a voice chat is received
def chat(bot, update):
    input_file  = 'voice.ogg'
    output_file = 'voice.wav'
    #Get download file of voice message
    file = bot.getFile(update.message.voice.file_id)
    print ("File received: " + str(update.message.voice.file_id))
    file.download(input_file)
    #Convert voice message from voice.ogg to voice.wav
    subprocess.call(['ffmpeg', '-i', input_file, output_file])

    #Convert from voice to text
    #client = Wit('wit.ai Access Token')
    resp = None
    with open(output_file, 'rb') as f:
      resp = client.speech(f, None, {'Content-Type': 'audio/wav'})

    #Map word version of numbers to int version of numbers
    p = inflect.engine()
    word_to_number_mapping = {}
    for i in range(1, 100):
        word_form = p.number_to_words(i)  # 1 -> 'one'
        word_to_number_mapping[word_form] = i

    #Response received from converting voice message to text
    if resp:
        if resp['_text']:
            text = resp['_text']
            #Split text by spaces
            data = text.split()
            #If first word is hello, then send message from BOT to user
            if data[0] == 'hello':
                update.message.reply_text(
                    'Warm Welcomes {} from the BOT'.format(update.message.from_user.first_name))
            #If first word is add, perform addition and send output from BOT to user
            elif data[0] == 'add':
                update.message.reply_text('Attempting ADDITION action')
                data.pop(0)
                sum = 0
                final = True
                #As numbers will be in text, convert those to ints and add those numbers together.
                for item in data:
                    if item in word_to_number_mapping:
                        ele = word_to_number_mapping[item]
                        sum += ele
                    else:
                        final = False
                if final:
                    update.message.reply_text('Addition is: {}'.format(sum))
                else:
                    update.message.reply_text('Could not perform ADDITION')
            #Bot cannot understand comand sent
            else:
                update.message.reply_text(
                    'BOT does not understand received instruction: {} '.format(str(resp['_text'])))
    #File cleanup
    os.remove(input_file)
    os.remove(output_file)

#Listener function which will execute if command: /hello is received.
def hello(bot, update):
    update.message.reply_text(
        'Hello {}'.format(update.message.from_user.first_name))

#Listener function which will execute if command: /todo is received.
def todo(bot, update, args):
    global todolist
    #If the first word after /todo is add, then add the item in the todo list.
    #Eg. /todo add item
    if args[0]=='add':
        todolist.append(str(args[1]))
        update.message.reply_text(
            'Added TODO: {}'.format(args[1]))
    #If the first word after /todo is view, then view the contents of the todo list.
    #Eg. /todo view
    elif args[0]=='view':
        for arg in todolist:
            update.message.reply_text(
                'TODO Item: {}'.format(arg))

#Listener function which will execute if command: /add is received.
def add(bot, update, args):
    sum = 0
    #Adds a series of numbers together.
    for arg in args:
        if not arg.isdigit():
            update.message.reply_text('Numbers supplied are not INT')
            return
        sum += int(arg)
    update.message.reply_text('Addition is: {}'.format(sum))

#Telegram updater object
updater = Updater()#Telegram Access Token inside
#Listener command so if command: /hello is received, function hello will execute
updater.dispatcher.add_handler(CommandHandler('hello', hello))
#Listener command so if command: /add is received, function add will execute
updater.dispatcher.add_handler(CommandHandler('add', add, pass_args=True))
#Listener command so if command: /todo is received, function todo will execute
updater.dispatcher.add_handler(CommandHandler('todo', todo, pass_args=True))
#Listener command so if voice message received, function chat will execute
updater.dispatcher.add_handler(MessageHandler(Filters.voice, chat))

updater.start_polling()
updater.idle()
