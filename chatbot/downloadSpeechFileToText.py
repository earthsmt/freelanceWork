from wit import Wit
import subprocess
import os

input_file = 'voice.ogg'
output_file = 'voice.wav'

exists = os.path.isfile(input_file)

if exists:
    subprocess.call(['ffmpeg', '-i', input_file, output_file])

    client = Wit('HTQCBQVDOOXN4RR222DQN6ATD5FWODPP')
    resp = None
    with open('voice.wav', 'rb') as f:
      resp = client.speech(f, None, {'Content-Type': 'audio/wav'})

    if resp:
        print('Yay, got Wit.ai response: ' + str(resp))
    os.remove(input_file)
    os.remove(output_file)
else:
    print('Input File does not exist')
