from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from wit import Wit
import subprocess
import os
import json
import inflect
todolist = []

def chat(bot, update):
    input_file  = 'voice.ogg'
    output_file = 'voice.wav'
    file = bot.getFile(update.message.voice.file_id)
    print ("File received: " + str(update.message.voice.file_id))
    file.download(input_file)

    subprocess.call(['ffmpeg', '-i', input_file, output_file])

    client = Wit('HTQCBQVDOOXN4RR222DQN6ATD5FWODPP')
    resp = None
    with open(output_file, 'rb') as f:
      resp = client.speech(f, None, {'Content-Type': 'audio/wav'})

    p = inflect.engine()
    word_to_number_mapping = {}
    for i in range(1, 100):
        word_form = p.number_to_words(i)  # 1 -> 'one'
        word_to_number_mapping[word_form] = i
    if resp:
        if resp['_text']:
            text = resp['_text']
            data = text.split()
            print(data[0])
            if data[0] == 'hello':
                update.message.reply_text(
                    'Warm Welcomes {} from the BOT'.format(update.message.from_user.first_name))
            elif data[0] == 'add':
                update.message.reply_text('Attempting ADDITION action')
                data.pop(0)
                sum = 0
                final = True
                for item in data:
                    if item in word_to_number_mapping:
                        ele = word_to_number_mapping[item]
                        sum += ele
                    else:
                        final = False
                if final:
                    update.message.reply_text('Addition is: {}'.format(sum))
                else:
                    update.message.reply_text('Could not perform ADDITION')
            else:
                update.message.reply_text(
                    'BOT does not understand received instruction: {} '.format(str(resp['_text'])))

    os.remove(input_file)
    os.remove(output_file)

def hello(bot, update):
    update.message.reply_text(
        'Hello {}'.format(update.message.from_user.first_name))

def todo(bot, update, args):
    global todolist
    if args[0]=='add':
        todolist.append(str(args[1]))
        update.message.reply_text(
            'Added TODO: {}'.format(args[1]))
    elif args[0]=='view':
        for arg in todolist:
            update.message.reply_text(
                'TODO Item: {}'.format(arg))

def add(bot, update, args):
    sum = 0
    for arg in args:
        if not arg.isdigit():
            update.message.reply_text('Numbers supplied are not INT')
            return
        sum += int(arg)
    update.message.reply_text('Addition is: {}'.format(sum))

updater = Updater('613725018:AAFQ2sVifdeMXFZktLlWxkMxcAQ_CiffZRw')
updater.dispatcher.add_handler(CommandHandler('hello', hello))
updater.dispatcher.add_handler(CommandHandler('add', add, pass_args=True))
updater.dispatcher.add_handler(CommandHandler('todo', todo, pass_args=True))
updater.dispatcher.add_handler(MessageHandler(Filters.voice, chat))

updater.start_polling()
updater.idle()
